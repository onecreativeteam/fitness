<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class WriteArticle extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (null!=\Auth::user()) {
           return true;
        }
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
               'name'=>'required|max:32',
                'slug'=>'required',
                'text'=>'min:15|required'
        ];
    }
}
