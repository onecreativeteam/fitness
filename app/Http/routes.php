<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index')->name('home.index');

Route::auth();

Route::get('/page/{page}','PagesController@show')->name('page.show');

Route::post('/contact','ContactController@contact')->name('contact.index');


//Routs for article section;
Route::get('/program/{category}','ProgramController@index')->name('view.category.index');
Route::get('/program/{category}/{program}','ProgramController@show')->name('view.category.program.show');;



//Routs for contact mails;
Route::get('/admin/mails','ContactMailController@index')->name('admin.mails');
Route::get('/admin/mails/{mail_id}','ContactMailController@show')->name('admin.mails.show');
//Routs for contact mails;

//Routs for product section;
Route::get('/shop/category/{category}','ProductCategoryController@show')->name('view.category');
Route::get('/shop/product/{product}','ProductController@show')->name('view.product');
Route::post('/shop/product/comment/{id}','CommentController@store')->name('product.comment.store');
Route::post('/product/add_prod/{id}',"ProductController@product_cart_add")->name('product.session.store');
Route::post('/product/remove/{index}',"ProductController@session_del")->name('product.session.delete');
//Routs for product section;


// Routes options


// Routes options



//Routs for admin section;
Route::resource('/admin','Admin\AdminController',['only'=>['index']]);

//Routs for Search section;

Route::get('/search/advanced/options','SearchController@advancedSearchResult')->name('search.advance.options.index');;
//Routs for Search section;

Route::resource('/shop/brand','BrandController',['only'=>['show']]);


Route::resource('/admin/programs/category', 'Admin\AdminProgramCategoryController',[
		'except'=>[
			'show'
		]
	]);
Route::resource('/admin/products/category', 'Admin\AdminProductCategoryController',[
		'except'=>[
			'show'
		]
	]);
Route::resource('/admin/products/brand', 'Admin\AdminProductBrandController',[
		'except'=>[
			'show'
		]
	]);
Route::resource('/admin/products', 'Admin\AdminProductController',[
		'except'=>[
			'show'
		]
	]);
Route::resource('/admin/programs', 'Admin\AdminProgramController',[
		'except'=>[
			'show'
		]
	]);
Route::resource('/admin/pages','Admin\AdminPagesController',[
		'except'=>[
			'show'
		]
	]);

Route::resource('/admin/users','Admin\AdminUsersController',[
		'except'=>[
			'create',
			'show',
			'store'
		]
	]);	

Route::resource('/admin/gallery','GalleryController',[
	'only'=>[
		'store',
		'destroy',
		'index'
	]]);

Route::get('/admin/option','OptionController@index')->name('admin.option.index');
Route::post('/admin/option','OptionController@update')->name('admin.option.update');
Route::resource('/slides','SlidesController',[
		'only'=>[
			'show',
			'update'
		]
	]);

Route::resource('/product-rating/rating','ProductRatingController',[
			'only'=>[
					'show',
					'store'
			]
	]);
Route::resource('/program-rating/rating','ProgramRatingController',[
			'only'=>[
					'show',
					'store'
			]
	]);
Route::resource('/profile','UserController');
