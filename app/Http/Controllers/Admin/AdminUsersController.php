<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Repositories\UserRepository as User;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminUsersController extends Controller
{

    /**
       * @var  App\Repositories\UserRepository $users
       */
      private $users;
      /**
       * @param App\Repositories\UserRepository $users
       * @return void;
       */

      public function __construct(User $users){
        $this->users=$users;
        $this->middleware(['auth','admin']);

      }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        public function index()
    {
        $result = $this->users->all();
        
        return view('admin.user.show',compact('result'));
    }

   

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $result=$this->users->findByAttr('id',$id);
        
        return view('admin.user.edit',compact('result'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $updatedUser = $this->users->updateByAttr('id',$id,$request->input());
        

        return redirect()->route('admin.users.index')->with('status',"$updatedUser->name has been successfully updated!!!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
    {
      $deletedUser = $this->users->destroyByAttr("id",$id);
         
        return redirect()->route('admin.users.index')->with('status',"$deletedUser->name has been deleted!!!");
    }
}
