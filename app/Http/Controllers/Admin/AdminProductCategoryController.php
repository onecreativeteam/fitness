<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Repositories\ProductCategoryRepository as Category;

use App\Http\Requests;

use App\Http\Requests\AddCategoryRequest;

use App\Http\Controllers\Controller;

class AdminProductCategoryController extends Controller
{
  /**
   * @var  App\Repositories\ProductCategoryRepository $categories
   */
  private $categories;
  /**
   * @param App\Repositories\ProductCategoryRepository $categories
   * @return void
   */
  public function __construct(Category $categories){
    $this->categories=$categories;
     $this->middleware(['auth','admin']);
  }
    /**
     * @return  \Illuminate\Http\Response
     */

	public function index(){
		$result=$this->categories->all();
		
		return view('admin.product_categories.index',compact('result'));
	}
    /**
     * @return  \Illuminate\Http\Response
     */
    public function create(){
    	return view('admin.product_categories.create');
    }
    /**
     * @param string $slug
     * @return  \Illuminate\Http\Response
     */

    public function edit($slug){

    	$result=$this->categories->findByAttr('slug',$slug);
    	return view('admin.product_categories.edit',compact('result'));
    }
    /**
     * @param string $slug ,
     *        Illuminate\Http\Request $request 
     * @return  \Illuminate\Http\Response
     */
    public function update($slug, Request $request){
    	$this->categories
           ->updateByAttr('slug',$slug,$request->all());
          
    	return redirect()->route("admin.products.category.index")->with("status",'Category has been successfully updated!!!!');
    }
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCategoryRequest $request){
    	$input=$request->all();

       foreach ($input as $name => $value) {
        if ($name != "_token" && $name != "_method") {
           if ($this->categories->findByAttr($name , $value)) {
               return response()->json(['status'=>false,"error"=> "$value  already exist!!"]);
           }
        }
       };
      $brand=$this->categories->create($input);
      return response()->json(['status'=>true,'success'=>"$brand->name has been added!!"]);
    }
     /**
     * Destroy 
     *
     * @param  string $request
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug){
      $category = $this->categories->destroyByAttr('slug',$slug);
    	return response()->json(['status'=>true,'success'=>"$category->name has been deleted!!"]);
    }
}
