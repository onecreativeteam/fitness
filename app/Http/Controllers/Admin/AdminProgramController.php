<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\ProgramRepository as Program;
use App\Repositories\ProgramCategoryRepository as Category;
use App\Http\Controllers\Controller;

use App\Services\FilesService;

class AdminProgramController extends Controller
{

   /**
    * @var App\Repositories\ProgramRepository
    */
   private $programs;
   /**
    * @var App\Repositories\ProgramCategoryRepository
    */
   private $categories;
   /**
    * @param App\Repositories\ProgramRepository $program 
    *        App\Repositories\ProgramCategoryRepository $category
    * @return void
    */
   public function __construct(Program $program, Category $category){
      $this->programs = $program;
      $this->categories = $category;
      $this->middleware(['auth','admin']);
   }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $programs = $this->programs->all();

        return view("admin.program.show",compact('programs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
          $categories = $this->categories->all()->lists("name",'id');
       if (count($categories)) {
         
            return view("admin.program.create",compact('categories'));
         
       }
          $models["program_cat"]="Категория";
          \Session::flash('added','Моля добавете поне една категория за статия!');
           return view("admin.category.create",compact("models"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\WriteArticle $request)
    {
       
         $file=$request->file('picture');
         $input=$request->input();
        
         $input['slug']=mb_strtolower($input['slug']);

         $input['image_path']= FilesService::uploadFile($file,'/art_img',[
                  "width"=>250,
                  "height"=>250
          ]);
        
         $newArticle = $this->programs->create($input);
         $newArticle->categories()->attach($request->input('category_id'));
        
         return redirect()->back()->with('status',"Program has been successfully created!!!");
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $program = $this->programs->findByAttr('slug',$slug);
        $categories = $this->categories->all()->lists("name",'id');

      if (\Auth::user()->role=="admin" || \Auth::user()->role=='mod') {
        return view("admin.program.edit",compact('categories','program'));
      }
      return view("program.edit",compact('categories'));
    }

    /**
      * Update the specified resource in storage.
      * @param string $slug , Requests\WriteArticle $request
      * @return  App\Http\Response
      */
    public function update($slug,Requests\WriteArticle $request){
        
         $program = $this->programs->findByAttr('slug',$slug);
        
         $file=$request->file('picture');

         $input=$request->input();
         $input['slug']=mb_strtolower($input['slug']);
         if (!is_null($file)) {
           $input['image_path']=FilesService::uploadFile($file,'/art_img',[
                          'width'=>250,
                          'height'=>250
            ]);
         }
         
         $oldArticle=$program->update($input);
         $program->categories()->sync([$request->input('category_id')]);
         

          
               return redirect()->route('admin.programs.index')->with('status',"Program has been successfully updated!!!");
      
   }

    /**
     * Remove the specified resource from storage.
      * @param string $post
      * @return  App\Http\Response
      */
   public function destroy($slug){
      $deletedProgram = $this->programs->destroyByAttr('slug',$slug);
    
      return redirect()->route('admin.programs.index')->with('status',"$deletedProgram->name has been successfully deleted !!!!");
   }
}
