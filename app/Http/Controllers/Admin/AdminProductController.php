<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\ProductRepository as Product;
use App\Repositories\BrandRepository as Brand;
use App\Repositories\ProductCategoryRepository as Category;
use App\Http\Requests;

use App\Services\FilesService;

class AdminProductController extends Controller
{
  
  /**
  * @var App\Repositories\ProductRepository $products
  * @var App\Repositories\BrandRepository $brands
  * @var App\Repositories\ProductCategoryRepository $categories
  */  
  private $brands;
  private $products;
  private $categories;
  /**
   * @param  App\Repositories\ProductRepository $product
   *         App\Repositories\BrandRepository $brand
   *         App\Repositories\ProductCategoryRepository $category
   * @return void  
   *
   */
   public function __construct(Product $product, Brand $brand, Category $category)
    {
       $this->middleware(['auth','admin']);
       $this->brands=$brand;
       $this->products=$product;
       $this->categories=$category;
      
    }
    /**
     * @return Illuminate\Http\Response
     */
   public function index(){
        $result = $this->products->all();

        return view('admin.product.index',compact('result'));

      }
    /**
     * @return Illuminate\Http\Response
     */
   public function create()
    {
    	 $brands= $this->brands->all()->lists('name','id');
    	 $category= $this->categories->all()->lists('name','id');
      
      
        return view('admin.product.create',compact('brands','category'));
     
    }
    /**
     * @param App\Http\Requests\AddProductRequest $request
     * @return Illuminate\Http\Response
     */

     public function store(Requests\AddProductRequest $request){

      		$file=$request->file("picture");
      		$input=$request->all();
          
          $input['picture'] = FilesService::uploadFile($file,'/product',[
                    'width'=>200,
                    'height'=>200,
            ]);
      		
          $input['slug']=mb_strtolower($input['slug']);

      		$newProduct = $this->products->create($input);
      		// $newProduct->categories()->attach($request->input('category_id'));
      		if ($newProduct) {
      			
      			return redirect()->route('admin.products.index')->with('status','New product has been successfully added!!!!');
      		}

      		
      	
      }

      /**
     * @param string $slug
     * @return Illuminate\Http\Response
     */

      public function edit($slug){

       $result = $this->products->findByAttr('slug',$slug);
      

        return view('admin.product.edit',compact('result'));

      } 

    /**
     * @param string $slug,
     *        App\Http\Requests\AddProductRequest   $request
     * @return Illuminate\Http\Response
     */
      public function update($slug,Requests\AddProductRequest $request){
      $product = $this->products->findByAttr('slug',$slug);
      
      $input = $request->input();
      if (!isset($input['promotion'])) $input['promotion']=0;

       if (null != $request->file('picture')) {
         
         $input['picture'] = FilesService::uploadFile($request->file('picture'),'/product',[
                    'width'=>200,
                    'height'=>200,
            ]);
         if ($product->picture!='/product/placeholder.png') {
           
          FilesService::destroyFile(public_path().'_html'.$product->picture); 
         }
       }
       $product->update($input);
     
        return redirect()->route('admin.products.index')->with('status',"$product->name has been successfully updated !!!!");

      }
    /**
     * @param string $slug,
     *        
     * @return Illuminate\Http\Response
     */

      public function destroy($slug){

          $product = $this->products->destroyByAttr("slug",$slug);
          if ($product->picture!='/product/placeholder.png') {
           
           FilesService::destroyFile(public_path().'_html'.$product->picture); 
         }
        return redirect()->route('admin.products.index')->with("status","$product->name has been successfully deleted!!!");
      }
}