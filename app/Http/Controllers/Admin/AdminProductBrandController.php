<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Repositories\BrandRepository as Brand;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddCategoryRequest;
class AdminProductBrandController extends Controller
{
  /**
   * @var App\Repositories\BrandRepository $brand;
   */
  private $brand;
  /**
   * @param App\Repositories\BrandRepository $brand;
   * @return void
   */

    public function __construct(Brand $brand){
      $this->brand=$brand;
       $this->middleware(['auth','admin']);
    }
    /**
     * @return \Illuminate\Http\Response
     */

  	public function index(){
  		$result = $this->brand->all();
  		return view('admin.brand.index',compact('result'));
  	}
    /**
     * @return \Illuminate\Http\Response
     */
  	public function create(){
  		return view('admin.brand.create');
  	}
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

  	public function store(AddCategoryRequest $request){
  		$input=$request->all();

       foreach ($input as $name => $value) {
        if ($name != "_token" && $name != "_method") {
           if ($this->brand->findByAttr($name ,$value)) {
               return response()->json(['status'=>false,"error"=> "$value  already exist!!"]);
           }
        }
       };
      $brand=$this->brand->create($input);
      return response()->json(['status'=>true,'success'=>"$brand->name has been added!!"]);
  	}
    /**
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */

  	public function edit($slug){
      $brand = $this->brand->findByAttr('slug' ,$slug);
  		return view('admin.brand.edit',compact('brand'));
  	}
    /**
     * @param  string $brand , Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
  	public function update($slug,Request $request){
  		$this->brand->updateByAttr('slug',$slug,$request->all());
      return redirect()->route("admin.products.brand.index")->with('status',"Brand has been successfully updated !!!");
  	}
     /**
     * @param  string $slug
     * @return \Illuminate\Http\Response
     */
  	public function destroy($slug){

  		$old_brand=$this->brand->destroyByAttr('slug',$slug);

      return response()->json(['status'=>true,"success"=>"$old_brand->name has been deleted!!"]);
  	}

}
