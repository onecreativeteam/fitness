<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Repositories\ProgramCategoryRepository as Category;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\AddCategoryRequest;


class AdminProgramCategoryController extends Controller
{
  /**
   * @var App\Repositories\ProgramCategoryRepository $categories
   */
  private $categories;

  /**
   * @param App\Repositories\ProgramCategoryRepository $categories
   * @return void
   */
  public function __construct(Category $category){
    $this->categories=$category;
     $this->middleware(['auth','admin']);
  }

    /**
     * @return \Illuminate\Http\Response;
     */
    public function index(){
        $result = $this->categories->all();
        return view('admin.program_categories.index',compact('result'));
    }

     /**
     * @return \Illuminate\Http\Response;
     */
    public function create(){
        return view('admin.program_categories.create');
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response;
     */
    public function store(AddCategoryRequest $request){
        $input=$request->all();

       foreach ($input as $name => $value) {
        if ($name != "_token" && $name != "_method") {
           if ($this->categories->findByAttr($name , $value)) {
               return response()->json(['status'=>false,"error"=> "$value  already exist!!"]);
           }
        }
       };
      $category = $this->categories->create($input);
      return response()->json(['status'=>true,'success'=>"$category->name has been added!!"]);
    }

    /**
     * @param  string $slug
     * @return \Illuminate\Http\Response;
     */
    public function edit($slug){
        $result=$this->categories->findByAttr('slug',$slug);
        
        return view('admin.program_categories.edit',compact('result'));
    }

    /**
     * @param  string $slug,
     *         \Illuminate\Http\Request $request;
     * @return \Illuminate\Http\Response;
     */
    public function update($slug,Request $request){
      $this->categories->updateByAttr('slug',$slug,$request->all());
     
      return redirect()->route("admin.programs.category.index")->with('status','Program Category has been successfully updated!!!');
    }

    /**
     * @param  string $slug
     * @return \Illuminate\Http\Response;
     */
    public function destroy($slug){
      $category = $this->categories->destroyByAttr('slug',$slug);
      
      return response()->json(['status'=>true,"success"=>"$category->name has been deleted!!"]);
    }
}
