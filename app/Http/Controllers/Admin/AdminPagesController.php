<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Repositories\PageRepository as Page;
use App\Repositories\FieldRepository as Field;
use App\Repositories\OptionRepository as Option;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Services\FilesService;

class AdminPagesController extends Controller
{
     
        private $pages;
        private $fields;
        private $options;
        /**
         * @param App\Repositories\PageRepository $page;
                  App\Repositories\FieldRepository $field;
                  App\Repositories\OptionRepository $option;
         */
        public function __construct(Page $page, Field $field, Option $option)
        {
            $this->middleware(['auth','admin']);
            $this->pages = $page;
            $this->fields = $field;
            $this->options = $option;
        }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $result = $this->pages->all();
        
        return view('admin.pages.index',compact('result'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

         return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data=$request->all();
        
        $data['slug']=$this->slugCreate($request->name);

        $page = $this->pages->create($data);
       
        $this->storeUpdateFields($page,$request);

        if ($request->get('has_slider')) {
            for ($i=0; $i < count($request->title) ; $i++) {

                $slide_data['title']= $request->title[$i];
                $slide_data['content']= $request->sl_content[$i];
                $slide_data['path']= FilesService::uploadFile($request->file[$i],'/uploads');
                
                $page->slides()->create($slide_data);
            }
        }


        return redirect()->route('admin.pages.index')->with('status','Page created!!!');        
    }

    /**
     * @param string $string
     * @return string $slug
     */

    public function slugCreate($string){
       $slug=str_replace(' ', '-', $string);
       $slug = mb_strtolower($slug);
       return $slug;
    }
   


 

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $page = $this->pages->findByAttr('id' , $id);
        $fields=$this->fields->findByAttr('page_id', $page->id);

        foreach ($fields as $key) {
            $result[$key->field_name]=$key->field_meta;
        }
                 
        if (count($fields)) {
           return view('admin.'.$page->template.'.edit',compact('result','page'));
        }
        return view('admin.default.edit',compact('result','page'));
        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $page = $this->pages->findByAttr('id' , $id);    
        $data=$request->all();
        
        $page->update($data);
        $this->storeUpdateFields($page,$request);
          
        return redirect()->route('admin.pages.index')->with('status','Page updated!!!');

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        $page = $this->pages->destroyByAttr('id' , $id);  
        return redirect()->back()->with('status','Page deleted!!!');
    }

    /**
     * @param App\Models\Model $page ,
     *        App\Http\Request;
     * @return Illuminate\Database\Eloquent\Model
     */
     public function storeUpdateFields($page,Request $request){
         $fields = \Config::get('pages.template.' . $page->template);
        
            foreach ($fields as $field) {
                $meta = $this->fields->firstOrNew(['field_name'=>$field ,'page_id'=> $page->id]);  
                $meta->field_meta=$request->get($field);
                $meta->save();
            }
      

    }
}
