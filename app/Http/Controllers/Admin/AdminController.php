<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Contracts\Validation\Validator;
use App\Http\Requests\AddProductRequest;
class AdminController extends Controller
{
    public function __construct()
    {
    	 $this->middleware(['auth','admin']);
    	
    }
 
    public function index()
    {
    	return view('admin.index');
    }
   
}
