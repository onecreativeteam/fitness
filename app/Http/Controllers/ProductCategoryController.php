<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\ProductCategoryRepository as ProductCategory;
use App\Repositories\ProductRepository as Product;

class ProductCategoryController extends Controller
{

    private $categories;
    private $products;

    public function __construct(ProductCategory $category, Product $products){
        $this->categories=$category;
        $this->product=$products;
    }

   

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $category = $this->categories->findByAttr('slug' , $slug);

        $products =   $category->products()->paginate(9);
      
        return view('shop.index',compact('category','products'));
    }

}
