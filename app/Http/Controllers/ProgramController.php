<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\ProgramRepository as Program;
use App\Repositories\ProgramCategoryRepository as Category;


class ProgramController extends Controller
{
   /**
    * @var App\Repositories\ProgramRepository
    */
   private $programs;
   /**
    * @var App\Repositories\ProgramCategoryRepository
    */
   private $categories;
   /**
    * @param App\Repositories\ProgramRepository $program 
    *        App\Repositories\ProgramCategoryRepository $category
    * @return void
    */
   public function __construct(Program $program, Category $category){
      $this->programs = $program;
      $this->categories = $category;
     
   }
   //  /**
   //    * 
   //    * @return  App\Http\Response
   //    */

   // public function index()
   //   {
   //    $categories = $this->categories->all();
   //    return view('categories.index',compact('categories',"obj"));
   //   }   

     /**
      * @param string $slug
      * @return  App\Http\Response
      */

   public function index($slug)
   {

      $category = $this->categories->findByAttr('slug',$slug);
   	  $result=$category->posts()->paginate(5);

      return view('program.index',compact('category','result'));
   }
   /**
      * @param string $category ,string $post
      * @return  App\Http\Response
      */
   public function show($category, $post)
   {
      $category = $this->categories->findByAttr('slug',$category);
      $program = $this->programs->findByAttr('slug',$post);

      return view('program.show',compact('category','program'));
   }
 

}
