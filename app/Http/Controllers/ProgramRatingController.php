<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ProgramRatingRepository as ProgramRating;
use App\Repositories\ProgramRepository as Program;

use App\Http\Requests;

class ProgramRatingController extends Controller
{
     /**
     * @var App\Repositories\ProgramRatingRepository  $rating 
     */
    private $rating;
    /**
     * @var App\Repositories\ProgramRepository  $program
     */
    private $program;
    /**
     * @param  App\Repositories\ProgramRatingRepository  $rating
     * @return void
     */
    public function __construct(ProgramRating $rating,Program $program){
        $this->rating = $rating;
        $this->program = $program;

    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        $input=$request->all();
        $input['rater_ip']=$request->ip();

        $record = $this->rating->storeUpdate([
            'rater_ip'=>$input['rater_ip'],
            'program_id'=>$input['program_id']
            ],$input);
       return response()->json(['status'=>'succes','response_msg'=>"Вие гласувахте с $record->rate !!!"]);        
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $program = $this->program->findByAttr('id' , $id);

        $rating = $program->rating;
        $rating_avg = $rating->avg('rate');
        return response()->json(['status' => 'succes', 'response_msg'=>$rating_avg]);
    }
   
}
