<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\OptionRepository as Option;
use App\Repositories\PageRepository as Page;
class OptionController extends Controller
{
    /**
     * @var App\Repositories\OptionRepository $options
     */
    private $options;
    /**
     * @var App\Repositories\PageRepository $pages
     */
    private $pages;
    /**
     * @param App\Repositories\OptionRepository $option, App\Repositories\PageRepository $page
     * @return void
     */
    public function __construct(Option $option, Page $page)
    {
        $this->options = $option;
        $this->pages = $page;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = $this->pages->all()->lists('name','id');
        $option= $this->options->all()->lists('option_value','option_key');
        return view('admin.option.edit',compact('pages','option'));
    }

 
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {  
       $this->storeUpdateOptions($request->all());
       return redirect()->back()->with("status",'Options has been saved !!!'); 
    }
        /**
         * @param \Illuminate\Http\Request  $data
         * @return status
         */

    public function storeUpdateOptions($data){

            foreach ($data as $key => $value) {


                if ($key!=='_token') {
                 
                    $record= $this->options->firstOrNew(['option_key'=>$key]);
                    $record->option_value=$value;
                    $record->save();
                }
              
            }
    }
}
