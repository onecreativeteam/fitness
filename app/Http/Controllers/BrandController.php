<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\ProductRepository as Product;


use App\Repositories\BrandRepository as Brand;

class BrandController extends Controller
{
    /**
     * @var App\Repositories\BrandRepository $brand
     */
	private $brand;
	/**
     * @var App\Repositories\ProductRepository $product
     */
	private $product;

	/**
	 * @param App\Repositories\BrandRepository $brand,
	 *		  App\Repositories\ProductRepository $product
	 * @return void
	 */

	public function __construct(Brand $brand ,Product $product)
	{
		$this->product = $product;
		$this->brand = $brand; 
	}
	/**
	 *  @param string $slug  		
	 * 	@return view
	 */
	public function show($slug)
	{	
		$brand = $this->brand->findByAttr('slug', $slug);	
		$products = $brand->products()->paginate(9);
		
		return view('shop.index_brand',compact('brand','products'));
	}
	/**
	 * @param string $slug , string $product
	 * @return view
	 */
 	public function product($slug ,$product){ 
 		$brand = $this->brand->findByAttr('slug', $slug);
 		$product = $this->product->findByAttr('brand_id',$brand->id);
 

        return view('shop.show',compact('product'));
    }

}
