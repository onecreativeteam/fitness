<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SlideRepository as Slide;
use App\Http\Requests;

use App\Services\FilesService;

class SlidesController extends Controller
{


   private $slides;

   public function __construct(Slide $slides){
      $this->slides=$slides;
   }
   /**
    * @param int $slide
    * @return Json response
    */
	public function show($slide){
		$slide=$this->slides->findByAttr('id' , $slide);
      return response()->json(['status'=>true,"result"=>$slide]);
	
	}
   /**
    * @param int $slide , App\Http\Request $request;
    * @return Json response
    */
   public function update($slide , Request $request){
   			
    $slide = $this->slides->findByAttr('id',$slide);
   
	    $data = $request->all();
	    $file = $request->file("image");
            
      if (null != $request->file("image")) {
         
         unlink('../public_html'.$slide->path);
         $data['path'] = FilesService::uploadFile($file,'/uploads');
       };
  	
      $slide->update($data);
            
			return response()->json(["status"=>true,"new_image"=>$data['path']]);


   }
}
