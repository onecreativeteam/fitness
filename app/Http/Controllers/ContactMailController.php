<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ContactMailRepository as ContactMail;

use App\Http\Requests;

class ContactMailController extends Controller
{

    /**
     * @var App\Repositories\ContactMailRepository $mails;
     */
    private $mails;

    /**
     * @param App\Repositories\ContactMailRepository $contactMail
     * @return void
     */
    public function __construct(ContactMail $contactMail){
        $this->mails=$contactMail;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $mails = $this->mails->all();

        return view('admin.contact.mails',compact('mails'));
    }

   

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $mail = $this->mails->findByAttr('id' , $id);
       if($mail->unread == 1){
        $data['unread']=0;
        $mail->update($data);
       }
       return view('admin.contact.mail_read',compact('mail'));
    }

   
}
