<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\CommentRepository as Comment;

class CommentController extends Controller
{
    /**
     * @var  App\Repositories\CommentRepository $comment
     */
    private $comment;
    /**
     * @param  App\Repositories\CommentRepository $comment
     * @return void
     */
    public function __construct(Comment $comment)
    {
      $this->comment = $comment;
    }

    /**
     * @param Illuminate\Http\Request $request
     */
  	public function store(Request $request,$id=0)
  	{
        $input=$request->input();
      if ( $id != 0 ) {
         $comment = $this->comment->findByAttr('id',$id);
          $input['comment_id']=$comment->id;
      }
        
  	
  		 

  		$newComment=$this->comment->create($input);
  		
  		return redirect()->back();
  		
  	}
    /**
     * @param int $id , Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response 
     */
    public function reply($id,Request $request)
    {
      $comment = $this->comment->findByAttr('id', $id);

      $input=$request->input();
     
  
      $this->comment->create($input);

      return redirect()->back();
      
    }
}
