<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\PageRepository as Page;
use App\Repositories\FieldRepository as Field;
use App\Repositories\OptionRepository as Option;

class PagesController extends Controller
{

    private $pages;
    private $options;
    private $fields;
    /**
     * Create a new controller instance.
     * @param App\Repositories\PageRepository $page,
     *        App\Repositories\OptionRepository $option, 
     *        App\Repositories\FieldRepository $field
     * @return void
     */
    public function __construct(Page $page, Option $option, Field $field)
    {
       $this->pages=$page;
       $this->options=$option; 
       $this->fields=$field;
    }
    /**
     * @param string $slug
     * @return Illuminate\Http\Response
     */
    public function show($slug){
        $result=$this->pages->findByAttr('slug' ,$slug);
        $slider=$result->slides()->orderBy("slide_index","asc")->get();
        $fields = $this->fields->findByAttr('page_id', $result->id);
       
        if ($fields) {
            foreach ($fields as $key => $value) {
        
            $result[$value->field_name]=$value->field_meta;
         }
        }

       
        return view('template.'.$result->template.'.index',compact('result',"slider"));

    }

}
