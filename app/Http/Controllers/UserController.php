<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Repositories\UserRepository as User;

use App\Services\FilesService;

class UserController extends Controller
{
      /**
       * @var  App\Repositories\UserRepository $users
       */
      private $users;
      /**
       * @param App\Repositories\UserRepository $users
       * @return void;
       */

      public function __construct(User $users){
        $this->users=$users;
        $this->middleware(['auth']);

      }

      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function show($id)
      {
           $user = $this->users->findByAttr('id' , $id);
           return view('user.show',compact('user'));
      }
      /**
       * @param int $id , App\Http\Request $request
       * @return Illuminate\Http\Response
       */
   		public function update(Request $request,$id)
   	{
      $user= $this->users->findByAttr('id' , $id);
      $inputData;
      
      if ($request->file() && null !== $request->file("picture") )  {
     
        $inputData['picture'] = FilesService::uploadFile($request->file("picture"),'/profile',
          [
          'width' => 350,
          "height" => 350
          ]);

        if ($user->picture !== "/profile/user-placeholder.jpg") {
             FilesService::destroyFile(public_path()."_html$user->picture"); 
        }
        $user->update($inputData);
        return response()->json(['status'=>"success",'response_msg'=>'Успешно обновихте профилната си снимка!!!!',"path"=>\URL::to($user->picture)]);
      }

      if(null !== $request->input('password')){
        $rules = array(
        'password' => 'required|min:3|confirmed',
        'password_confirmation' => 'required|min:3|same:password'
        );
        $inputData= $request->input();
      
         $validator = \Validator::make($inputData, $rules);
          if ($validator->passes()) {

             $inputData['password']=bcrypt($inputData['password']);
              $user->update($inputData);
              return response()->json(['status'=>"success",'response_msg'=>'Успешно обновихте паролата си!!!!']);
          }
          return response()->json(['status'=>"error",'response_msg'=>$validator->errors()]);
      }
      if(null !== $request->input('email')){
        $inputData= $request->input();
          $rules = array(
        'email' => 'required|min:3|unique:users|email'
        );
        $validator = \Validator::make($inputData, $rules);
        if ($validator->passes()) {
           $user->update($inputData);
              return response()->json(['status'=>"success",'response_msg'=>'Успешно обновихте майла си!!!!',"place"=>".mail",'content'=>$user->email]);
        }
        return response()->json(['status'=>"error",'response_msg'=>$validator->errors()]);
      }
      if(null !== $request->input('phone')){
        $inputData= $request->input();
          $rules = array(
        'phone' => 'required|min:10|numeric'
        );
        $validator = \Validator::make($inputData, $rules);
        if ($validator->passes()) {
           $user->update($inputData);
              return response()->json(['status'=>"success",'response_msg'=>'Успешно обновихте майла си!!!!',"place"=>".phone",'content'=>$user->phone]);
        }
        return response()->json(['status'=>"error",'response_msg'=>$validator->errors()]);
      }


      $inputData  = $request->all();

      $user->update($inputData);
      return response()->json(['status'=>"success",'response_msg'=>'Успешно обновихте профила си!!!!',"place"=>".address",'content'=>$user->address]);
   	}
  
}
