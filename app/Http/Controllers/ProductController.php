<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ProductCategoryRepository as Category;
use App\Http\Requests;
use App\Repositories\ProductRepository as Product;
use App\Models\Comment;


class ProductController extends Controller
{
    private $products;
    private $categories;
    /**
     * @param App\Repositories $product
     */
    public function __construct(Product $product, Category $category){
      $this->products = $product;
      $this->categories= $category;
    }

    /**
     * @return  \Illuminate\Http\Response
     */

  	public function index()
  	{
      $category = ['name'=>'Продукти'];
  		$products = $this->products->paginate(9);
  		
  		return	view('shop.index',compact('products','category'));
  	}

    /**
     * @param string $slug
     * @return  \Illuminate\Http\Response
     */
    public function show($slug){
        $product= $this->products->findByAttr('slug', $slug);
        $comments= $product->direct_comments;

        return view('shop.show',compact("product",'comments'));
    }


      /**
       *  @param int $id
       *  @return JSON response;
       */
     public function product_cart_add($id){
        $product = $this->products->findByAttr('id',$id);
        if ( empty(\Session::get('product'))) {
           \Session::push('product',["id"=>$product->id,"name"=>$product->name,"price"=>$product->price,"quantity"=>1]);
        }else{
            
         $products= \Session::get('product');

         if ( !in_array($id, array_column($products,"id"))) {
         \Session::push('product',["id"=>$product->id,"name"=>$product->name,"price"=>$product->price,"quantity"=>1]);
         }else{

            $products[array_search($id, array_column($products,"id"))]['quantity']+=1;
            \Session::put('product',$products);

         }
        }
      

        if (\Session::has('sum')) {
            $sum = \Session::get('sum');
            $sum+=$product->price;
            \Session::put('sum',$sum);

        }else{
           \Session::put('sum',$product->price);

        }

        return response()->json(['success'=>true,"product"=>$product]);
    }
    
     public function session_del($index){
        $val = \Session::get('product');
        $sum = \Session::get('sum');
        $sum-=($val[$index]['price']*$val[$index]['quantity']);
        
        unset($val[$index]);
          \Session::put('sum',$sum);
          \Session::put('product',$val);
         return response()->json(['success'=>true,"newsum"=>$sum]);
      
    }
  

}
