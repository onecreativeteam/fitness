<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Repositories\OptionRepository as Option;
use App\Repositories\ContactMailRepository as ContactMail;

class ContactController extends Controller
{
	/**
	 * @var  App\Repository\OptionRepository $options
	 *		 App\Repository\OptionRepository $mails	
	 */
	private $options;
	private $mails;
	/**
	 * @param App\Repository\OptionRepository $option
	 *		  App\Repository\OptionRepository $mails	
	 * @return void
	 */
	public function __construct(Option $option,ContactMail $mails){
			$this->options=$option;
			$this->mails=$mails;
		}	
	/**
	 * @param App\Http\Requests $request
	 * @return Illuminate\Http\Response
	 */	

 	public function contact(Request $request){
 		// dd($request->get('mail'));
 		$data['from']=$request->get('mail');
 		$data['to']=$this->options->findByAttr('option_key' , "mail");
 		$data['content']=$request->get('mail_cont');
 		
 		$adminData['sender'] = $request->get('mail');
 		$adminData['subject'] = $request->get('theme');
 		$adminData['text'] = $request->get('mail_cont');
 		
 		try {
 			$mail = \Mail::send('mail.contact',$data,function($message) use ($data){
			 			$message->from($data['from']);
			 			$message->to($data['to']->option_value)->subject("Съобщение от потребител!!");
			 		});
			
			if (! $mail) {
				throw new Exception('Съобщението не беше изпратено!!');
		 		} 
		 	$this->mails->create($adminData);

 			
 		} catch (Exception $e) {
 		return redirect()->back()->with('msg',$e->getMessage());
 		}

 		return redirect()->back()->with('msg','Вие успешно изпратихте съобщение към администратора!');
 	}
}
