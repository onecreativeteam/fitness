<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ProductRepository as Product;
use App\Repositories\ProductCategoryRepository as ProductCategory;
use App\Repositories\BrandRepository as Brand;
use App\Repositories\PageRepository as Page;
use App\Repositories\ProgramRepository as Program;
use App\Repositories\ProgramCategoryRepository as ProgramCategory;

use App\Http\Requests;

class SearchController extends Controller
{

	private $products;
	private $programs;
	private $brands;
	private $pages;
	private $productCategories;
	private $programCategories;

	public function __construct(Product $product, ProductCategory $productCategory, Brand $brand, Page $page, Program $program,ProgramCategory $programCategory){
		$this->products = $product;
		$this->programs = $program;
		$this->brands = $brand;
		$this->pages = $page;
		$this->productCategories = $productCategory;
		$this->programCategories = $programCategory;
	}

		public function index(){
			return view('search.index');			
		}
		

		
		public function advancedSearchResult(Request $request){
				$pages=[];
				$programs=[];
				$products=[];
				$search = $request->input('search');
				$options=$request->all();
				$_options['page']=$request->input('page');
				$_options['brand_id']=$request->input('brand_id');
				$_options['products']=$request->input('products');
				$_options['programs']=$request->input('programs');
				// unset($options['search']);
					$_options = array_filter($_options);
				if (count($_options)) {
			
				
					if (!isset($_options['brand_id'])) {
						$_options['brand_id']=[];
					}
					if (!isset($_options['products'])) {
						$_options['products']=[];
					}
					if (!isset($_options["programs"]) ) {
						$_options['programs']=[];
					}
					if (!isset($_options['page'])) {
						$_options['page']=[];
					}
					
						if (count($_options['page'])) {
							$pages = $this->searchInPages($search);	
	
						}
						if (count($_options["programs"])) {
							$programs = $this->searchInPrograms($search,$_options["programs"]);

						}
						if (count($_options["products"]) || count($_options["brand_id"])) {
						$products = $this->searchInProducts($search,$_options["products"],$_options["brand_id"]);	
							
						}
						
				}else{
					
					$pages = $this->searchInPages($search);	
					$programs = $this->searchInPrograms($search);
					$products = $this->searchInProducts($search);	
				}
						
				
		
				
				
			
			
					
	

			$result=array_merge($pages,$programs,$products);

			$results=collect($result);
			
			if (isset($options["sort"]) && $options["sort"]==1) {
					$results=$results->sortBy('created_at');
					$options['sort_type']='Oldest';
					$options["sort"]=0;
			}else{
				
				$results=$results->sortByDesc('created_at');
					$options["sort"]=1;
					$options['sort_type']='Latest';
			}
			
			
			$result["products"]=$this->productCategories->all()->lists('name', "id");
			$result["programs"]=$this->programCategories->all()->lists('name', "id");
			$result["brand_id"]=$this->brands->all()->lists('name', "id");
			// $options['search']=$search;
			return view('search.index', compact('results','result','sort','options'));		



				
		}
	/**
	*
	* @param string $searchText ,array $params
	* @return array 
	*/
	private function searchInPages($searchText,$params=[]){
		if (!count($params)) {
		    return $this->pages->whereOrWhere(['content','name'] , $searchText)
						   ->get()->toArray();
			}
			
	}
	/**
	*
	* @param string $searchText ,array $params
	* @return array 
	*/	
	private function searchInPrograms($searchText,$params=[]){
			if (!count($params)) {
				return	$this->programs->whereOrWhere(['text','name'] , $searchText)->with('categories')
											->get()->toArray();
			}

			
			$programs = $this->programs->whereOrWhere(['text','name'] , $searchText)->with('categories')
											->get();
			
			 return collect($this->findInRelation($programs,$params))->toArray();
	}
	/**
	*
	* @param string $searchText ,array $params array $brand_id
	* @return array 
	*/
	private function searchInProducts($searchText,$params=[],$brand_id=[]){
		$products = $this->products->whereOrWhere(['name',"description"], $searchText)
											->get();

		if (!count($params) && !count($brand_id)) {
				return	$products->toArray();

			}
		if (!count($brand_id)) {
					return $products->filter(function($key) use ($params){
									return in_array($key->category_id,$params);
							})->toArray();						
							}
		if (count($brand_id)  && count($params)) {
			$_products=[];
			$_byCategory = $products->filter(function($key) use ($params){
									return in_array($key->category_id,$params);
							})->toArray();
			$_byBrand = $products->filter(function($key) use ($brand_id){
									return in_array($key->brand_id,$brand_id);
							})->toArray();
							return array_unique(array_merge($_byCategory,$_byBrand), SORT_REGULAR);



			}
			return $products
					->filter(function($key) use ($brand_id){
						return in_array($key->brand_id,$brand_id);
					})
					->toArray();
		}


		private function findInRelation($collection, $params){
			$result=[];
			
				foreach ($collection as $item ) {
					foreach ($item->categories as $_item) {
						if (in_array($_item->id, $params)) {
							$result[]=$item;
						}	
					}
				}

				return $result;
		}

}
