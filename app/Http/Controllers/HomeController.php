<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Repositories\PageRepository as Page;
use App\Repositories\OptionRepository as Option;
use App\Repositories\FieldRepository as Field;

class HomeController extends Controller
{
    private $pages;
    private $options;
    private $fields;
    /**
     * Create a new controller instance.
     * @param App\Repositories\PageRepository $page,
     *        App\Repositories\OptionRepository $option, 
     *        App\Repositories\FieldRepository $field
     * @return void
     */
    public function __construct(Page $page, Option $option, Field $field)
    {
       $this->pages=$page;
       $this->options=$option; 
       $this->fields=$field;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      
       $result=null;
       $option = $this->options->findByAttr('option_key' , "home");
       
       if ($option) {
         $result = $this->pages->findByAttr('id' , $option->option_value);

         $slider= $result->slides()->orderBy("slide_index","asc")->get();
  

         return view('template.'.$result->template.'.index',compact('result','slider'));
       }
       return view('template.default.index',compact('result','slider'));
    }

    
        
}
