<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\GalleryRepository as Gallery; 

use App\Http\Requests;

use App\Services\FilesService;

class GalleryController extends Controller
{
    /**
     * @var App\Repositories\GalleryRepository $gallery
     */
    private $gallery;
    /**
     * @param App\Repositories\GalleryRepository $gallery
     * @return void
     */
    public function __construct(Gallery $gallery){
        $this->gallery=$gallery;
        $this->middleware(['auth','admin']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   	public function index()
   	{
   	     $pictures = $this->gallery->paginate(9);

   		
   		return view('admin.gallery.index',compact('pictures'));
   	}

      
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Json\Response
     */ 
   	public function store(Request $request){
   		$files = $request->file('file');
		   			if ( ! is_null($files)){
                  $input['name']=explode('.',$files->getClientOriginalName())[0];
		   			$input['img_path']= FilesService::uploadFile($files,'/image',[
                      'width'=>450,
                      'height'=>450
              ]);
		   			$upload_success= $this->gallery->create($input);
                   if( $upload_success ) {
                      return \Response::json('success', 200);
                   } else {
                    return \Response::json('error', 400);
                    }
	   		}
   	}
    /**
     * @param int $id
     * @return \Illuminate\Http\Response
     */
   	public function destroy($id){
   		$picture = $this->gallery->destroyByAttr('id' , $id);

   		if ($picture) {
   			FilesService::destroyFile(public_path().'_html'.$picture->img_path);
   			return redirect()->route('admin.gallery.index')->with('status',"Picture has been successfully deleted from our database!!!!");
   		}

   	}
   	
}
