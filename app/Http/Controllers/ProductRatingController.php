<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Repositories\ProductRatingRepository as ProductRating;
use App\Repositories\ProductRepository as Product;

use App\Http\Requests;

class ProductRatingController extends Controller
{
    /**
     * @var App\Repositories\ProductRatingRepository  $rating 
     */
    private $rating;
    /**
     * @var App\Repositories\ProductRepository  $product
     */
    private $product;
    /**
     * @param  App\Repositories\ProductRatingRepository  $rating
     * @return void
     */
    public function __construct(ProductRating $rating,Product $product){
        $this->rating = $rating;
        $this->product = $product;

    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        
        $input=$request->all();
        $input['rater_ip']=$request->ip();

        $record = $this->rating->storeUpdate([
            'rater_ip'=>$input['rater_ip'],
            'product_id'=>$input['product_id']
            ],$input);
       return response()->json(['status'=>'succes','response_msg'=>"Вие гласувахте с $record->rate !!!"]);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->product->findByAttr('id' ,$id);
        $rating = $product->rating;
        $rating_avg = $rating->avg('rate');
        return response()->json(['status' => 'succes', 'response_msg'=>$rating_avg]);
    }

    
}
