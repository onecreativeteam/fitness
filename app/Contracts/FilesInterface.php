<?php 

namespace App\Contracts;

interface FilesInterface
{
	/**
	* @param file $file , array $dimensions
	* @return string
	*/ 
	
	public static function uploadFile($file,$path, $dimensions = []);

	/**
	* @param string $path
	* @return
	*/ 
	
	public static function destroyFile($path);
}