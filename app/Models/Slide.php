<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
	protected $fillable=[
				'slidable_id',
				'slide_index',
				'slidable_type',
				'title',
				'content',
				'path',

	];
   	public function slidable(){
   			return $this->morphTo();
   	}
}
