<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactMail extends Model
{
	protected $table = 'contact_mails';
	protected $fillable = [
		'sender',
		'subject',
		'text',
		'unread',
	];
}
