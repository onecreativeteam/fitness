<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProgramRating extends Model
{
	protected $table = 'program_rating';
    protected $fillable = [
    	'rater_ip',
    	'program_id',
    	'rate'
    ];
    public function program(){
    	return $this->belongsTo(Program::class,'program_id');
    }
}
