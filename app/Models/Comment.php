<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
   	protected $fillable = [
   			
   			'auther',
   			'comment',
   			'product_id',
   			'comment_id',
   			];

   	public function product(){
   		return $this->belongsTo(Product::class,'product_id');
   	}

   	public function getCommentsAttribute(){
   		 return \App\Models\Comment::where(['comment_id' => $this->id])->get();
   	}

}
