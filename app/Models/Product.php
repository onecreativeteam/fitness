<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $table = "products";
    protected $fillable = [
    	'name',
    	'description',
        'slug',
    	'picture',
    	'price',
    	'promotion',
    	'brand_id',
        "category_id",
        'quantity'

    ];

    public function brand(){
        return $this->belongsTo(Brand::class,'brand_id');
    }

    public function categories()
    {
    	return $this->belongsTo(Products_Category::class,"category_id");
    }

    public function rating()
    {
        return $this->hasMany(ProductRating::class,'product_id','id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'product_id','id');
    }

    public function getDirectCommentsAttribute(){
        return $this->comments()->where(['comment_id' => 0])->get();
    }

    public function slides(){
                  return $this->morphMany(Slide::class,'slidable');
              }

    /**
    * 
    * @return
    */ 
    
    public function orders(){
        return $this->belongsToMany(Order::class, 'order_product', 'product_id',"order_id");
    }

}
