<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductRating extends Model
{
    protected $table = 'product_rating';
    protected $fillable = [
    		'product_id',
    		'rater_ip',
    		'rate'
    ];

   	public function product()
   	{
   		$this->belongsTo(Product::class,'product_id');
   	}
}
