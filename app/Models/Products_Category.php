<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Products_Category extends Model
{		
    	protected $table="products_categories";
   		protected $fillable=['name','slug'];


   		public function products()
   		{
   			 return $this->hasMany(Product::class,"category_id",'id');
   		}
}
