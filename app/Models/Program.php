<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
 
    protected $fillable = [
    	'name','slug','image_path','text','short_descript'

    ];
    public function rating()
    {
        return $this->hasMany(ProgramRating::class,'program_id','id');
    }
    public function categories()
    {
    	return $this->belongsToMany(Program_Category::class,'program_category','program_id','category_id');
    }
}
