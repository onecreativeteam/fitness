<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{

	public $timestamps  = false;
  	protected $fillable = [
  					"name",
  					'nav_index',
  					'tittle',
  					'slug',
  					'template',
  					'meta_descr',
  					'content',
            "has_slider"
  						];
              public function slides(){
                  return $this->morphMany(Slide::class,'slidable');
              }
}
