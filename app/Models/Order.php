<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable=[
    	'user_id',
    	'status'
    ];

    /**
    * @param
    * @return
    */ 
    
    public function products(){
    	 return $this->belongToMany(Product::class,'order_product','order_id','product_id');
    }
}
