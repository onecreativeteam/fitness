<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program_Category extends Model
{
	protected $table = "programs_categories";
	protected $fillable=["name","slug"];



	public function posts()
	{
		return $this->belongsToMany(Program::class ,'program_category','category_id','program_id');
	}

}
