<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
    			'page_id',
    			'field_name',
    			'field_meta'
    ];
}
