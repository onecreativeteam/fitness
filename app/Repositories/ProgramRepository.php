<?php
namespace App\Repositories;
use App\Repositories\Repository;
use App\Models\Program;
class ProgramRepository extends BaseRepository
{
	/**
	* @var App\Models\Program $modelClass;
	*/
	protected $modelClass=Program::class;
}