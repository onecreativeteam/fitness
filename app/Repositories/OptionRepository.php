<?php
namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Option;

class OptionRepository extends BaseRepository
{
	/**
	* @var App\Models\Option $modelClass;
	*/
	protected $modelClass = Option::class;
	
}