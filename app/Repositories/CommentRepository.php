<?php 
namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Comment;

class CommentRepository extends BaseRepository
{
	/**
	 * @var App\Models\Comment;
	 */
	protected $modelClass=Comment::class;
}