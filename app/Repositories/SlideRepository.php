<?php
namespace App\Repositories;
use App\Repositories\Repository;
use App\Models\Slide;
class SlideRepository extends BaseRepository
{
	/**
	* @var App\Models\Slide $modelClass;
	*/
	protected $modelClass=Slide::class;
}