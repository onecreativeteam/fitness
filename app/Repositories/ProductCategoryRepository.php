<?php 
namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Products_Category as Category;

class ProductCategoryRepository extends BaseRepository
{
	/**
	 * @var App\Models $modelClass
	 */
	protected $modelClass = Category::class;
}