<?php 
namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Field;

class FieldRepository extends BaseRepository
{
	/**
	* @var App\Models\Field $modelClass;
	*/
	protected $modelClass = Field::class;
	/**
     * @param string $attr, string $collumn
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findByAttr($attr,$collumn){
        return $this->getModel()->where($attr,$collumn)->get();
    }
}