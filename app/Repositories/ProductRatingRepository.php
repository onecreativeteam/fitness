<?php
namespace App\Repositories;
use App\Repositories\Repository;
use App\Models\ProductRating;
class ProductRatingRepository extends BaseRepository
{
	/**
	* @var App\Models\ProductRating $modelClass;
	*/
	protected $modelClass=ProductRating::class;
}