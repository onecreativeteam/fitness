<?php 
namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Gallery;

class GalleryRepository extends BaseRepository
{
	/**
	 * @var  App\Models\Gallery $modelClass;
	 */
	protected $modelClass = Gallery::class;
}