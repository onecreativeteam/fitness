<?php
namespace App\Repositories;
use App\Models\Brand;
use App\Repositories\Repository;

class BrandRepository extends BaseRepository
{
	/**
	* @var App\Models $modelClass;
	*/
	protected $modelClass= Brand::class;
}