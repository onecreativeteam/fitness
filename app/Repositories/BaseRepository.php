<?php 

namespace App\Repositories;

use Illuminate\Database\DatabaseManager;
use Illuminate\Database\Query\Builder;
use Illuminate\Log\Writer;
use Illuminate\Events\Dispatcher;

/**
 * Class BaseRepository
 *
 * @package App\Repositories
 */
class BaseRepository extends Repository
{

    /**
     * BaseRepository constructor.
     * @param 
     * @param 
     * @param 
     */
    public function __construct()
    {
        
       
    }
/**
     * @param string $attr, string $collumn
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function findByAttr($attr,$collumn){
        return $this->getModel()->where($attr,$collumn)->first();
    }
    /**
     * @param mixed $collumn, string $searchText
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function whereOrWhere($collumn=[],$searchText){
        return $this->getModel()->where($collumn[0], 'LIKE' ,'%'.$searchText.'%')->orWhere($collumn[1], 'LIKE' ,'%'.$searchText.'%');
    }
    /**
     * @param mixed $array
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function with($array=[]){
        return $this->getModel()->with($array);
    }
    /**
     * @param mixed $data ,string $attr, string $collumn
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function updateByAttr($attr,$collumn,$data=[]){
        $model = $this->findByAttr($attr,$collumn);
        $model->update($data);
        return $model;
    }
    /**
     * @param string $attr, string $collumn
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function destroyByAttr($attr,$collumn){
        $model = $this->findByAttr($attr,$collumn);
        $model->delete();
        return $model;
    }
    /**
     *  @param mixed $collumns 
     *  @return \Illuminate\Database\Eloquent\Model
     */    
    public function findOrNew($collumns=[]){
        return $this->getModel()->firstOrNew($collumns);
    }

    /**
     * Store or update given record
     */
    public function storeUpdate($collumns=[],$data=[]){
         $model = $this->findOrNew($collumns);
         foreach ($data as $key => $value) {
            if ($key != '_token' && $key != '_method') {
                $model->$key = $value;
            }
           
         }
         $model->save();
         return $model;
    }


}