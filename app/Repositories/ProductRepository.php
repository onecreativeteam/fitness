<?php 
namespace App\Repositories;

use App\Repositories\Repository;
use App\Models\Product;

class ProductRepository extends BaseRepository
{
	/**
	 * @var App\Models $modelClass;
	 */
	protected $modelClass = Product::class;
}