<?php
namespace App\Repositories;
use App\Repositories\Repository;
use App\Models\User;
class UserRepository extends BaseRepository
{
	/**
	* @var App\Models\User $modelClass;
	*/
	protected $modelClass=User::class;
}