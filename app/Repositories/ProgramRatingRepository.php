<?php
namespace App\Repositories;
use App\Repositories\Repository;
use App\Models\ProgramRating;
class ProgramRatingRepository extends BaseRepository
{
	/**
	* @var App\Models\ProgramRating $modelClass;
	*/
	protected $modelClass=ProgramRating::class;
}