<?php
namespace App\Repositories;
use App\Repositories\Repository;
use App\Models\Program_Category;
class ProgramCategoryRepository extends BaseRepository
{
	/**
	* @var App\Models\Program_Category $modelClass;
	*/
	protected $modelClass=Program_Category::class;
}