<?php
namespace App\Repositories;
use App\Repositories\Repository;
use App\Models\ContactMail;
class ContactMailRepository extends BaseRepository
{
	/**
	* @var App\Models\ContactMail $modelClass;
	*/
	protected $modelClass=ContactMail::class;
	public function all()
    {
        return $this->getModel()->orderBy("created_at",'desc')->get();
    }

}