<?php

namespace App\Providers;
use App\Models\Program;
use App\Models\Product;
use App\Models\Program_Category;
use App\Models\Products_Category;
use Illuminate\Support\ServiceProvider;

class ViewComposer extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
            view()->composer('parts.nav',function($view)
            {
                $view->with('art_cat' , Program_Category::all());
            });

            view()->composer('admin.gallery.edit',function($view){
                $view->with('result',\App\Models\Gallery::all());
            });

            view()->composer('admin.product.form',function($view)
            {
                $view->with('brands',\App\Models\Brand::all()->lists('name','id'));
                $view->with('category',\App\Models\Products_Category::all()->lists('name','id'));
            });

            view()->composer('admin.pages._form',function($view){
                foreach (\Config::get('pages.template') as $key => $v) {
                  
                    $templates[$key]=$key;

                }
          
                $view->with('templates',$templates);
            });

            view()->composer('admin._nav',function($view)
            {
                $view->with('admin',\Auth::user());
            });
            view()->composer('admin.side_nav',function($view)
            {
                $mails= new \App\Repositories\ContactMailRepository;
                $view->with('new', $mails->where(['unread'=>1]));
            });

           
            
            view()->composer('parts.side_menu_programs','App\Composers\ProgramMenuComposer');

            view()->composer('parts.side_menu_shop','App\Composers\ProductMenuComposer');

            view()->composer('parts.side_menu','App\Composers\DefaultMenuComposer');

           
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
