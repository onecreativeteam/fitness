<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
/*
        \Route::bind("slide",function ($slide)
        {
            return \App\Models\Slide::where('id',$slide)->firstOrFail();
        });
        

        \Route::bind("article_category",function ($cat)
        {
            return \App\Models\Program_Category::where('slug',$cat)->firstOrFail();
        });
       
        \Route::bind("product_category",function ($cat)
        {
            return \App\Models\Products_Category::where('slug',$cat)->firstOrFail();
        });
        \Route::bind("product_category",function ($slug)
        {
            return \App\Models\Products_Category::where('slug',$slug)->firstOrFail();
        });

        \Route::bind("program_category",function ($slug)
        {
            return \App\Models\Program_Category::where('slug',$slug)->firstOrFail();
        });

         \Route::bind("product",function ($slug)
        {
            return \App\Models\Product::where('slug',$slug)->firstOrFail();
        });
          \Route::bind("post",function ($slug)
        {
            return \App\Models\Program::where('slug',$slug)->firstOrFail();
        });
          \Route::bind("page",function ($page)
        {
            return \App\Models\Page::where('slug',$page)->firstOrFail();
        });

          \Route::bind("pages",function ($id)
        {
            return \App\Models\Page::where('id',$id)->firstOrFail();
        });
          \Route::bind("brand",function ($slug)
        {
            return \App\Models\Brand::where('slug',$slug)->firstOrFail();
        });*/


    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $this->mapWebRoutes($router);

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    protected function mapWebRoutes(Router $router)
    {
        $router->group([
            'namespace' => $this->namespace, 'middleware' => 'web',
        ], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
