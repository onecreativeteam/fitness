<?php 
namespace App\Services;

use App\Contracts\FilesInterface;

class FilesService implements FilesInterface{
 /**
  * @param file $file, string $path;
  * @return string $path
  */
//To do add dimensions for the resize method
	 public static function uploadFile($file, $path,$dimensions=[]){
         if (isset($file) && substr($file->getMimeType(), 0,5)=='image') {
            $extension = $file->getClientOriginalExtension();
            $file_name = time()."-".str_random(4).".".$extension;
            $upload_path = public_path()."_html".$path;
            $file->move($upload_path,$file_name);
            if(count($dimensions)){
                $image= \Image::make(sprintf($upload_path.'/%s',$file_name))->resize($dimensions['width'],$dimensions['height'])->save();
            }
            return $path.'/'.$file_name;
          }else{
            return $path.'/placeholder.png';
          } 
      }
      /**
      * @param string $path
      * @return
      */ 
      
      public static function destroyFile($path){
        
        unlink($path);
      }
}
