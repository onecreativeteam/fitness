<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Repositories\UserRepository as User;

class AdminPolicy
{
    use HandlesAuthorization;
    
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
     
    }
    public function edit($user){
      
       
        return $user->role === 'admin';
    }
}
