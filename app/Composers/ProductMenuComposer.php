<?php
namespace App\Composers;

use App\Repositories\ProductCategoryRepository as Category;	
use App\Repositories\ProductRepository as Product;

class ProductMenuComposer 
{
	/**
	* @var App\Repositories\ProductRepository $product
	*/	
	private $products;

	/**
	* @var App\Repositories\ProductCategoryRepository $categories
	* 
	*/	
	private $categories ;

	/**
	 * @param App\Repositories\ProductCategoryRepository $category;
	 *        App\Repositories\ProductRepository $product;
	 * @return void	
	 */
	public function __construct(Product $product, Category $category)
	{
		$this->products = $product;
		$this->categories = $category;
	}

	/**
	* @param view object @view
	* @return view object @view
	*/ 
	
	public function compose($view){

		$categories = $this->categories->with('products')->get();
		$products = $this->products->with('rating')->get();

		foreach ($categories as $key => $category) {
					foreach ($category->products as $product) {
						$category->avg_rate = $product->rating->avg('rate');
					}
				}	
		$categories = $categories->sortByDesc('avg_rate');

		foreach ($products as $key => $product) {
			$product->avg_rate = $product->rating->avg('rate');
		}
	
		$product_rate = $products->sortByDesc('avg_rate')->take(5);

		$view->with('product_popular_category', $categories);
        $view->with('product_rating', $product_rate);
	}

}