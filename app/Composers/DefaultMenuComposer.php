<?php 
namespace App\Composers;

use App\Repositories\ProgramRepository as Program;
use App\Repositories\ProductRepository as Product;

class DefaultMenuComposer
{
    /**
    * @var App\Repositories\ProgramRepository $programs
    * 
    */    
    private $programs;

    /**
    * @var App\Repositories\ProductRepository $products
    * 
    */    
    private $products;
    
    /**
     * @param App\Repositories\ProgramRepository $program;
     *        App\Repositories\ProductRepository $product;    
     * @return void
     */
    public function __construct(Program $program, Product $product)
    {
     $this->programs = $program;
     $this->products = $product;
    }

    /**
    * @param  mixed $view
    * @return view
    */ 
    
    public function compose($view){
        $programs = $this->programs->with('rating','categories')->get();
        $products = $this->products->with('rating')->get();   
            foreach ($programs as $key => $program)
            {
                $program->category_slug = $program->categories[0]->slug;
                $program->avg_rate = $program->rating->avg('rate');
            }
        $programs = $programs->sortByDesc('avg_rate')->take(5);

        foreach ($products as $key => $product) {
                $product->avg_rate = $product->rating->avg('rate');
        }
        $products = $products->sortByDesc('avg_rate')->take(5);

        $view->with('program_rated',$programs);
        $view->with('product_rated',$products);
    }
}