<?php 
namespace App\Composers;

use App\Repositories\ProgramCategoryRepository as Category;
use App\Repositories\ProgramRepository as Program;


class ProgramMenuComposer
{
	/**
	 * @var App\Repositories\ProgramCategoryRepository $categories
	 */
		private $categories;

	/**
	 * @var App\Repositories\ProgramRepository $programs
	 */
		private $programs;

	/**
	*  @param App\Repositories\ProgramCategoryRepository $categories
	*		  App\Repositories\ProgramRepository $program
	*  @return void
	*/	
	public function __construct(Category $category, Program $program){
		$this->categories=$category;
		$this->programs=$program;
	}

	/**
	 * @param object $view
	 * @return ...
	 */
	public function compose($view){
		$categories = $this->categories->with('posts')->get();
                foreach ($categories as $key => $category) {
                    foreach ($category->posts as $post) {
                            $category->avg_rate=$post->rating->avg('rate');
                    }
                      
                }
                $categories=$categories->sortByDesc('avg_rate');
                $program = $this->programs->with('rating','categories')->get();
                
                foreach ($program as $key => $value) {
                        $value->category_slug = $value->categories[0]->slug;
                        $value->avg_rate = $value->rating->avg('rate');
                }
                $program_rate = $program->sortByDesc('avg_rate')->take(5);
                
                $view->with('program_popular_category', $categories);
                $view->with('program_rating', $program_rate);
		
	}

}