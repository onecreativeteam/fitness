var animation;
$(document).ready(function() {
	function nextPic () {
		var slideAct = slider.find('.active');
	if (slideAct.next().length>0) {
		slideAct.animate({opacity:"0"},1000,function (argument) {
			$(this).addClass('wait').removeClass('active');
		slideAct.next().removeClass('wait').addClass('active').animate({opacity:"1"},1000);
		})

	}else{
		
		slideAct.animate({opacity:"0"},1000,function (argument) {
			$(this).addClass('wait').removeClass('active');
			slideAct.parent().children().first().removeClass('wait').addClass('active').animate({opacity:"1"},1000);
		})
	};
}

function prevPic () {
		var slideAct = slider.find('.active');
	if (slideAct.prev().length>0) {
		
		slideAct.animate({opacity:"0"},1000,function (argument) {
			$(this).addClass('wait').removeClass('active');
	slideAct.prev().removeClass('wait').addClass('active').animate({opacity:"1"},1000);
		})

	}else{
		
		slideAct.animate({opacity:"0"},1000,function (argument) {
			$(this).addClass('wait').removeClass('active');
			slideAct.parent().children().last().removeClass('wait').addClass('active').animate({opacity:"1"},1000);
		})
	};
}



	

	var slider = $("#slider-cont");
	if(slider.length>0){

		var slides=slider.find('[s="slide"]');
		var slideWidth = slider.width()+20;
		for (var i = 0; i<slides.length; i++) {
			if (slides[i].className!="active") {
				slides[i].setAttribute("style", "opacity:0")
			};
		};
		var nextBtn = $("[data-type='next']");
		var prevBtn = $("[data-type='prev']");
		nextBtn.on('click', nextPic);
		prevBtn.on('click', prevPic);

	}



animation = setInterval(nextPic, 6000);
	slider.mouseenter(function(event) {
		clearInterval(animation);
	});
	slider.mouseleave(function(event) {
		animation = setInterval(nextPic, 6000);
	});

});
