window.onload = function(){
	 $.ajaxSetup({
     	   headers: {
       	    		 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
     		   }
  	 		 });

	var template = document.getElementById('template') ;
	if (template!==null) {
		template.onchange=	function(){
			templateView(this.value, temps,opt);

		}

	var temps = {
		default: {} ,
		contact:    {latitude: ["text","Latitude"]
					,longitude: ["text","Longitude"]
					,address: ["text","Адрес"]
					,phone: ["text","Телефон"]
					,mail: ["mail","Е-майл"]}
				}
	
}
	checkHeight();
	var body = $('body');
	var opt = document.getElementById('temps')
	var selectBox = document.getElementById('controller');
	var last= document.getElementById('last');
	var container1 = document.createElement("DIV");
		container1.setAttribute('class','form-control');
	var container = document.createElement("DIV");
		container.setAttribute('class','form-control');
		container.setAttribute('id','optional');
	var label = document.createElement('LABEL');
		label.setAttribute('for','picute');
		label.appendChild(document.createTextNode('Снимка:'));
	var field = document.createElement('INPUT');
		field.setAttribute('type','file');
		field.setAttribute('name','picture');
		field.setAttribute('id','picture');
		container.appendChild(label);
		container.appendChild(field);
	
	if (selectBox != null) {
		check(selectBox,last,container);
		selectBox.onchange = function(){
		
		check(selectBox,last,container);
		
		}
	}
/**
* submitting table form for reading the customer mail;
* @var DOM row 
*/
if ($(".readMsg").length) {
	// @param default event -> e
	$('.readMsg').on('click',function(e){
		e.preventDefault();
		$(this).find('form').submit();
	})
}
/*STORE NEW RECORD FOR PRODUC CATEGORY, BRAND AND PROGRAM CATEGORY*/
if( $('[data-btn="store"]').length  || $('[data-btn="edit"]').length ){
	form=$('[data-btn="store"]').length ? $('[data-btn="store"]').parent() : $('[data-btn="edit"]').parent();
	nameInput=form.find("[name='name']");
	slugInput=form.find("[name='slug']");
	nameInput.on("keyup",function(key){
				
					slugInput.val(nameInput.val().toLowerCase());
					slugInput.val(slugInput.val().split(' ').join('_'));
		})
}
if(	$('[data-btn="store"]').length ){
	
	$('[data-btn="store"]').on('click',function(e)
		{
			e.preventDefault();
			

			laravelRequest={
					url: form.attr("action"),
					type: form.find("[name='_method']").length ? form.find("[name='_method']").val() : form.attr("method"),
					dataType:"json",
					data: form.serialize(),
				}
			// laravelURL = form.attr("action");
			// laravelMethod= form.find("[name='_method']").length ? form.find("[name='_method']").val() : form.attr("method");
			// laravelData = form.serialize();
			brandCreate = categoryAjax(laravelRequest);/*laravelURL,laravelMethod,laravelData*/
			brandCreate.success(function(response){
				if (response.status) {
					feedback= response.success.toString()
					notification($('#success_msg'),feedback);
					form[0].reset();
					return 0;
				}
				feedback= response.error.toString()
				
				notification($('#error_msg'),feedback);
				
			});	
			brandCreate.fail(function(error){
				var _feedback="";
				
					obj = $.parseJSON(error.responseText);
				for(var a in obj){
					_feedback+= "<p>"+obj[a].toString()+"</p>";
				}
				notification($('#error_msg'),_feedback);
			});
		})
		
}


/*DELETE ITEM*/
if( $('[data-btn="destroy"]').length ){
	$('[data-btn="destroy"]').on('click',function(e)
		{
			e.preventDefault();
			form=$(this).parent();
			laravelRequest={
				url: form.attr("action"),
				type: form.find("[name='_method']").length ? form.find("[name='_method']").val() : form.attr("method"),
				dataType:"json",

			}
			destroyCategory = categoryAjax(laravelRequest);
			destroyCategory.success(function(response){
				if (response.status) {
					feedback= response.success.toString()
					console.log(feedback)
					notification($('#success_msg'),feedback);
					form.parent().parent().remove();
					return 0;
				}
				
			})
		});
}



//*Simple Jquery Ajax*//
var categoryAjax = function(request){
	return $.ajax(request)
}

	/*show add slider*/
		if ($('[name="has_slider"]').length) {
				
				var slider=$('[name="has_slider"]');
					var father = slider.parent();
				slider.on('change',"",function(){
						//check if its clicked


					
						if(this.checked){
							slideCount=1;

							//show inputs for slider 
							var inputBlock =  '<div id="slides-input" style="display:none">'+ 
												'<a href="#" class="btn anim" data-btn="add-slide">Добави слайд</a>'+
												'<div class="form-control">'+
												"<label for='slide-"+slideCount+"'>Слайд "+slideCount+':</label>'+
												'<input type="file" name="file[]" id="slide"'+slideCount+'"></div>'+
												'<div class="form-control">'+
												"<label for='title-"+slideCount+"'>Заглавие за слайд "+slideCount+':</label>'+
												'<input type="text" name="title[]" id="title"'+slideCount+'"></div>'+
												'<div class="form-control">'+
												"<label for='content-"+slideCount+"'>Съдържание за слайд "+slideCount+':</label>'+
												'<input type="text" name="sl_content[]" id="content"'+slideCount+'"></div>'+
												'<div class="form-control">'+
												"<label for='index-"+slideCount+"'>Индекс на слайд "+slideCount+':</label>'+
												'<input type="number" name="sl_index[]" id="index"'+slideCount+'"></div>'+
												'</div></div>'
												
												
							//appent to the father element
							father.append(inputBlock);
							father.find('#slides-input').slideDown(400);
						}else{
							var el = $(this).parent().find('#slides-input')
							el.slideUp(400).promise().done(function(){
								el.remove()
							});
							
						}

					father.on('click','[data-btn="add-slide"]',function(e){
							e.preventDefault();
							slideCount++;
							addNew = '<div class="form-control">'+
									"<label for='slide-"+slideCount+"'>Слайд "+slideCount+':</label>'+
									'<input type="file" name="file[]" id="slide"'+slideCount+'">'+
									"<label for='title-"+slideCount+"'>Заглавие за слайд "+slideCount+':</label>'+
									'<input type="text" name="title[]" id="title"'+slideCount+'">'+
									"<label for='content-"+slideCount+"'>Съдържание за слайд "+slideCount+':</label>'+
									'<input type="text" name="sl_content[]" id="content"'+slideCount+'"></div>'
								
							father.find('#slides-input').append(addNew)

					})
				})
		}
	/*Slider edit*/
	if ($('#slider_edit').length) {

			var slidesURL = APP_URL+'/slides/';
			var slider_edit = $('#slider_edit');
				slider_edit.on('click','[data-btn="edit-slide"]',function(e){
					e.preventDefault();
						console.log(APP_URL)
						 modal=body.find('#modal');
						
						var slide_ID = $(this).parent().attr('data-id');
					
						var slide = ajaxRequest("GET",slidesURL,slide_ID);
							slide.success(function(data){
							
								modal.find("img").attr("src", APP_URL+data.result.path);
								modal.find("[name='id']").attr("value",data.result.id);
								modal.find("[name='file']").attr("value",data.result.path);
								modal.find("[name='title']").attr("value",data.result.title);
								modal.find("[name='content']").attr("value",data.result.content);
							}) 
							$(this).colorbox({ 
								inline:true,											 
								width:"50%",
								onComplete: function(){
									$.colorbox.resize();
								}
							});
						//Ajax get correct slide;
					
				})

	// $('[data-btn="ajax"]').on('click',function(e){
	/*  slide update */	
	$( '#updateForm' )
			.submit( function( e ) {
				e.preventDefault();
				slide_id = modal.find("[name='id']").attr("value");

				var fd = new FormData(this)

				$.ajax({
				  url: APP_URL+'/slides/'+slide_id,
				  xhr: function() { //getting the XHR 

				       var xhr = new XMLHttpRequest();
				       var total = 0;

				       // Get the total size of files
				       $.each(document.getElementById('image').files, function(i, file) {
				              total += file.size;
				       });

				       // Called when upload progress changes. xhr2
				       xhr.upload.addEventListener("progress", function(evt) {
				              // show progress like example
				              var loaded = (evt.loaded / total).toFixed(2)*100; // percent

				            
				       }, false);

				       return xhr;
				  },
				  type: 'POST',
				  processData: false,
				  contentType: false,
				  data: fd,
				  success: function(data) {
				       // do something...
				     $('#modal').colorbox.close();
				     $(".slide_preview[data-id='"+slide_id+"']").find("img").attr("src", APP_URL+data.new_image);	
					  notification($('#success_msg'));
				  }
			});
	});
	}
	
/*Notification for the user*/

var notification = function(ele,feedback=null){
	if (feedback!=null) {
		ele.find('#feedback').empty().append(feedback);
	}
	ele.animate({
		right:0
	},1000).delay(2000).animate({
		right:'-28%'
	},1000)
}


	window.onresize = function(){
		checkHeight();
	}
	
//tinymce.init({ selector:'#textBox' });

}

	function check(el,last,cont){
		if (el.selectedIndex==2) {
				
				return last.appendChild(cont);
				
			}
		return cont.remove();
	}


	function checkHeight(){
		var win = window.innerHeight;
		var body = document.getElementsByClassName('adm-wrapper')[0];
// console.log(win);
// 		console.log(body.clientHeight);
		if (win > body.clientHeight) {
			body.style.height=win+"px";
		}
	}

	function templateView(el,obj,place,container){
			//console.log(obj[el]	)
			while (place.firstChild) place.removeChild(place.firstChild);		
			if (el) {
				var form=obj[el]
				for (var key in form) {
					var line;
					container = document.createElement("DIV");
					container.setAttribute('class','form-control');

						label=document.createElement('label');
						label.setAttribute("for",key);
						label.innerHTML = form[key][1];
					

						line= (form[key][0]==='textarea') ? document.createElement('textarea'):document.createElement('input');
						line.type=form[key][0];
						line.name=key;
						line.id=key;
						container.appendChild(label)
						container.appendChild(line)
						place.appendChild(container);
					//console.log(line)
				}
			}else{
				while (place.firstChild) place.removeChild(place.firstChild);		
			}
	}	
//AJAX FOR SLIDES UPDATE
	var getSlide = function(id){
		return $.get('/slides/'+id,'json');
	
	}

	var ajaxRequest = function(type , getURL , id , data=null ){
		
			return $.ajax({
				type: type,
				url: getURL + id,
				data: data,
				dataType: 'json',	
				success:function(response){
						console.log(response.result);
				}

			});
	
	}

	function handleData(data) {
		return data;
	}