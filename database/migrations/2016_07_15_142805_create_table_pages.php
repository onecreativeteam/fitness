<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name');
            $table->integer('nav_index');
            $table->string('slug');
            $table->string('tittle');
            $table->string('template');
            $table->text('content');
            $table->integer('has_slider')->unsigned();
            $table->string('meta_descr');
           

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
