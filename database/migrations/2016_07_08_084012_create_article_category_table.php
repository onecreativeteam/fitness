<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticleCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('program_category', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('program_id')->unsigned();
                $table->foreign('program_id')->references('id')->on('programs')->onDelete('cascade');
            $table->integer('category_id')->unsigned();
                $table->foreign('category_id')->references('id')->on('programs_categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('program_category');
    }
}
