
{!! Form::open(['action'=>['CommentController@store']]) !!}
	{!! Form::hidden('product_id',$product->id) !!}
	
	<div class="form-group">
		{!! Form::label('auther','Име:') !!}
		{!! Form::input('text','auther',null,['class'=>'form-control','placeholder'=>'Име'])!!}
	</div>
	<div class="form-group">
		{!! Form::label('comment',"Коментар:") !!}
		{!! Form::textarea('comment',null,['class'=>"form-control",'rows'=>'3','required'=>'required'])!!}
	</div>
	{!! Form::submit('Коментирай',['class'=>'btn-custom btn-large text-center']) !!}
{!! Form::close() !!}