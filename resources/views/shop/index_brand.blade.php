@extends('layouts.app')

@section('title')
	{{$brand->name}}
@stop

@section('meta_info')
	This is an example of a meta description. This will often show up in search results.
@stop

@section('content')

	<section class="col-sm-12 col-md-8 section"><!--SECTION-->
					<div class="row">
						<h1 class="header-border margin-top-res">{{$brand->name}}</h1>
					</div>
					<div class="row p-m-reset push-down custom-width">
					@If(count($products)>0)
						@foreach($products as $product)
								<div class="col-xs-4 pr-border product-marg">
									<div class="row header-border">
										<h6 class=" text-center">
											<a href="{{action('ProductController@show',[$product->slug])}}" data-name="prName">{{$product->name}}</a>
										</h6>
									</div>
									<a href="{{action('ProductController@show',[$product->slug])}}" class="text-center">{{ Html::image($product->picture,"", array('class' => 'img-thumbnail img-responsive pull-down push-down')) }}</a>
									<p class="text-center">Цена:<span class="price" data-pr="pr-1">{{$product->price}}</span><span class="currency">лв.</span></p>
									<p class="text-center">
										<a class="btn-custom" data-id='{{$product->id}}' data-ident="btn-add">Купи</a>
										<a href="{{action('ProductController@show',[$product->slug])}}" class="btn-custom">Информация</a>
									</p>
									
								</div>

						@endforeach
					@else
						<h4 >Няма налични продукти.</h4>
					@endif
				</div>
@include('parts.pagination')
	</section>



@stop
@section('side_nav')
		@include('parts.side_menu_shop')
@stop