@extends('layouts.app')

@section('content')


	
	<section class="col-md-8 section"><!--SECTION-->
					<div class="row">
						<h1 class="header-border margin-top-res">{{$product->name}}</h1>
					</div>
					<div class="row push-down">
						<div class="col-sm-5 text-center">{{ Html::image( $product->picture, "",array('class' => 'img-responsive img-thumbnail push-down')) }} </div>
						<div class="col-sm-7">
							<ul class="contact-list">
								
								<li>
									<span class="">Марка: <a class='link' href="{{action('BrandController@show',$product->brand->slug)}}">{{$product->brand->name}}</a> </span>
								</li>
								<li>
									<span class="">Налично количество: 
										@if($product->quantity)
											<span>{{$product->quantity}}</span> 
										@else
										  	<span class="text-danger">Скоро в наличност</span> 
										@endif
										
									
									</span>
								</li>
								<li>
									{!! Form::open(['action'=>['ProductRatingController@store']]) !!}
											
												{!! Form::input('hidden','product_id',$product->id) !!}
											<ul class="rating-box">
												<li><a href="" class="glyphicon glyphicon-star rating" data-rate='1'></a></li>
												<li><a href="" class="glyphicon glyphicon-star rating" data-rate='2'></a></li>
												<li><a href="" class="glyphicon glyphicon-star rating" data-rate='3'></a></li>
												<li><a href="" class="glyphicon glyphicon-star rating" data-rate='4'></a></li>
												<li><a href="" class="glyphicon glyphicon-star rating" data-rate='5'></a></li>
												<li> Гласувай с <span class='will-rate'>0</span> звезди</li>
												<br>
												<li> Райтинг:&nbsp;<span class='current-rate'>{{$product->rating->avg('rate')}}</span></li>
											</ul>
									{!! Form::close() !!}
								</li>
								<li class="response-li-msg"></li>
								<li>
									<span class="des-price">Цена: {{$product->price}} лв.</span>
								</li>

							</ul>
							@if($product->quantity)
								<a data-id='{{$product->id}}' data-ident="btn-add" class="btn-custom btn-large text-center">Купи Сега</a>
							@endif
							
						</div>
					</div>
					<div class="row push-down">
						<h2 class="header-border  margin-top-res">Описание</h2>
						<p class="push-down col-xs-12 para"> {{$product->description}}</p>
					</div>
						@include('comments.index')
				</section>



@stop

@section('side_nav')
		@include('parts.side_menu_shop')
@stop