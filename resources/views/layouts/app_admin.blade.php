<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>FitnesQuest</title>
    <script type="text/javascript">
			var APP_URL = "{!! URL::to('/') !!}";
			
	</script>
    
	 {!! Html::style("css/admin/adm-style.css") !!}
	 {!! Html::style("css/colorbox.css") !!}
	 @yield('special')
	
	
</head>
<body >


<main class="main container">
	
	@include('admin._nav')
	@include('admin.side_nav')
	
	<section class='adm-wrapper'>
	@if(session('status'))
		<div class="alert success">
        	{{ session('status') }}
        </div>
	@endif
	@yield('content') 
	</section>
</main>
	  {!! Html::script("js/jquery.min.js") !!}
 	  {!! Html::script("js/admin/adm-java.js") !!}
 	  {!! Html::script('js/jquery.color-min.js') !!}
</body>
			<div id="success_msg" class="success_response">
				<p id="feedback">Upload has been successful</p>
				<img src="{{url('/image/success.png')}}" alt="">
			</div>
			<div id="error_msg" class="error_response">
				<p id="feedback"></p>
				<img src="{{url('/image/error.png')}}" alt="">
			</div>
</html>
