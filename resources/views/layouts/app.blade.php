<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @if(isset($result->meta_descr) )
     <meta name="description" content="{{$result->meta_descr}}">  
    @endif
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>FitnesQuest -
    @if(isset($result->tittle))
     {{$result->tittle}}
    @endif
     </title>
    {!! Html::style("css/bootstrap.css") !!}
    {!! Html::style("css/colorbox.css") !!}
    {!! Html::style("css/style.css") !!}
    @if( isset($result->has_slider) && $result->has_slider)
    {!! Html::style("css/flexslider.css") !!}
    @endif
    @yield('special')

   
</head>
<body id="app-layout">
    @include('parts.nav')
       
     @if( isset($result->has_slider) && $result->has_slider)
        
         @include('parts.slider')
    @endif
    <main class="main container">
   
       
    @yield('content')
    @yield('side_nav')
    </main>
    @include('parts.footer')
    {!! Html::script('js/jquery.min.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}
    {!! Html::script('js/custom-js.js') !!}
    
    {!! Html::script('js/jquery.color-min.js') !!}
@if( isset($result->has_slider) && $result->has_slider)
    {!! Html::script('js/jquery.flexslider-min.js') !!}
    
@endif
    
</body>
</html>
