
<aside class="col-sm-12 col-md-4 reset-right">
					<div class="aside">
							
						
						@if(count($product_popular_category))
						<h5 class="text-left _menuHead  header-border margin-top-res" data-toggle="collapse"  data-target="[data-cl='product_category']" >Най-популярни категории<span class="glyphicon glyphicon-plus pull-right "></span></h5>
						<ul class="list-unstyled category collapse" data-cl='product_category'>
							@foreach($product_popular_category as $category)	
							<li>
								<a href="{{ action('ProductCategoryController@show',$category->slug) }}">{{$category->name}}</a>
							</li>
							@endforeach
						</ul>
						@endif
						@if(count($product_rating))
						<h5 class="text-left _menuHead header-border margin-top-res" data-toggle="collapse"  data-target="[data-cl='product']" >Най-Популярни продукти<span class="glyphicon glyphicon-plus pull-right "></span></h5>
						<ul class="list-unstyled category collapse" data-cl="product">
							@foreach($product_rating as $rate)	
							<li>
								<a href="{{ action('ProductController@show',$rate->slug) }}">{{$rate->name}}</a>
							</li>
							@endforeach
						</ul>
						@endif
						
					</div>
				</aside>