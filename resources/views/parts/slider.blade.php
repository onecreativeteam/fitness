
<div class="flexslider">
  <ul class="slides">
	 	 @foreach($slider as $slide)
	   		<li>
	   		{{ HTML::image($slide->path) }}
	  	  	</li>
	    @endforeach
  </ul>
</div>