
<aside class="col-sm-12 col-md-4 reset-right">
					<div class="aside">
							
						
						
						@if(count($program_popular_category))
						<h5 class="text-left _menuHead header-border margin-top-res" data-toggle="collapse"  data-target="[data-cl='program_category']" >Най-популярни категории<span class="glyphicon glyphicon-plus pull-right "></span></h5>
						<ul class="list-unstyled category collapse" data-cl="program_category">
							@foreach($program_popular_category as $category)	
							<li>
								<a href="{{ action('ProgramController@index',$category->slug) }}">{{$category->name}}</a>
							</li>
							@endforeach
						</ul>
						@endif
						
						@if(1)
						<h5 class="text-left _menuHead header-border margin-top-res" data-toggle="collapse"  data-target="[data-cl='program']" >Най-Популярни програми<span class="glyphicon glyphicon-plus pull-right "></span></h5>
						<ul class="list-unstyled category collapse" data-cl="program">
							@foreach($program_rating as $category)	
							<li>
								<a href="{{ action('ProgramController@show',[$category->category_slug,$category->slug]) }}">{{$category->name}}</a>
							</li>
							@endforeach
						</ul>
						@endif
					</div>
				</aside>