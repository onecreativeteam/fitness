
<aside class="col-sm-12 col-md-4 reset-right">
					<div class="aside">
							
						
						
						@if(count($product_rated))
						<h5 class="text-left _menuHead header-border margin-top-res" data-toggle="collapse"  data-target="[data-cl='product']" >Най-Популярни продукти<span class="glyphicon glyphicon-plus pull-right "></span></h5>
						<ul class="list-unstyled category collapse" data-cl="product">
							@foreach($product_rated as $rate)	
							<li>
								<a href="{{ action('ProductController@show',$rate->slug) }}">{{$rate->name}}</a>
							</li>
							@endforeach
						</ul>
						@endif
						@if(1)
						<h5 class="text-left _menuHead header-border margin-top-res" data-toggle="collapse"  data-target="[data-cl='program']" >Най-Популярни програми<span class="glyphicon glyphicon-plus pull-right "></span></h5>
						<ul class="list-unstyled category collapse" data-cl="program">
							@foreach($program_rated as $category)	
							<li>
								<a href="{{ action('ProgramController@show',[$category->category_slug,$category->slug]) }}">{{$category->name}}</a>
							</li>
							@endforeach
						</ul>
						@endif
					</div>
				</aside>