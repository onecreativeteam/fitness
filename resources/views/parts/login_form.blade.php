{!! Form::open(["url"=>'login',"class"=>"inner-form"]) !!}
			@if (count($errors))
    			@include('errors._err')
			@endif
			<h4 class="header-border margin-top-res row">Вход</h4>
			<div class="form-row">
			{!!Form::label("email","Е-Майл")!!}
			{!!Form::email("email",null,["class"=>"cs-input"])!!}
			</div>

			<div class="form-row">
			{!!Form::label("password","Парола")!!}
			{!!Form::password("password",["class"=>"cs-input"])!!}
			</div>

			{{ Html::link('/register', 'Регистрация', array("class" => "btn-custom btn-large reset-width"))}}
			{!!Form::submit("Вход",['class'=>'btn-custom btn-large'])!!}
{!! Form::close() !!}