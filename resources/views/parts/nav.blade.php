
<header class="custom-colored custom-font-color font-style header">
	<div class="container">
	<!--CART-->
		<div class="row text-right pos-rel">
			<div class="container pos-abs cart-box media-cart " id="cart-box" style="display:none;">
					<div class="table-responsive">
					<span class="text-right price-all"><span class="glyphicon glyphicon-remove point-cursor" id="close"></span></span>
						<table class="table table-hover cart-max" id="cart-all">
							<thead>
								<tr>
									<th colspan="2">Продукт</th>
									<th>Брой</th>
									<th>Цена</th>
								</tr>
							</thead>
									
						
							<tbody id="cart-body" >
							@if(\Session::has('product'))
								
								@foreach(\Session::get('product') as $index => $line )

								

								<tr data-prodid="{{ array_get($line, 'id') }}" class="cart-row">
										<td>
											<span class="glyphicon glyphicon-remove point-cursor" data-del="{{$index}}" data-btn="del"></span>
										</td>
										<td>
											{{ array_get($line, 'name') }}
										</td>
										<td>
											{!! Form::input('number',"quantity",array_get($line, 'quantity'),['class'=>'custom-input quantity-box']) !!}
										</td>
										<td>
											{{array_get($line, 'price')}}
										</td>
										
									</tr>
								@endforeach
							@endif
							</tbody>
						</table>
						<span class="text-right price-all">Общо:<span data-sum="0" id="price-all" class="price-bd">
						@if(\Session::has('sum')) 
							{{\Session::get('sum')}}
						@else
						 	 0	 
						@endif 

						</span>  лв</span>
								<p class="text-right">
								@if(!Auth::check())
									{{ Html::link('/login', 'Напред', array("class" => "btn-custom cust-width"))}}
								@else
									{{ Html::link('', 'Напред', array("class" => "btn-custom cust-width"))}}
								@endif
								</p> 
						
					</div>
				</div>
<!--CART-->

						<div class="col-xs-7 col-sm-4">

							<a href="{!! action('HomeController@index') !!}"><img src="{!! asset(Config::get('pages.logo.path')) !!}" class="img-responsive logo" alt="{!! Config::get('pages.logo.title') !!}"></a>
						</div>
						<button type="button" class=" glyphicon glyphicon-plus btn-customize visible-xs  pos-abs media-plus" data-toggle="collapse" data-target=".menu-drop"></button>

						<div class="col-xs-7 col-sm-8 hidden-xs pos-rel">
							@if(!Auth::check())
							<ul class="list-unstyled user-menu">
							
								<li><a class="user-in point-cursor" data-tg-1="toggle" data-tg-to="#login-form">Вход</a></li>
								<li></li>
								<li><a href="{{url('/register')}}" class="user-in point-cursor" >Регистрация</a></li>
							</ul>
							
							<div class="pos-abs login-form text-center" id="login-form" style="display:none">
								<!--Input Form-->
								
									 @include('parts.login_form')

								<!--Input Form-->
							</div>	
							@else
							<p class="white_link">Здравей <a href="{{action('UserController@show',Auth::user()->id)}}" class="white_link">{{ Auth::user()->name}}</a> | <a href="{{ url('/logout')}}" class="white_link">Изход</a> 
								@if(\Auth::user()->role === "admin" || \Auth::user()->role === "mod")
									| <a href="{{ url('/admin')}}" class="white_link">Edit</a>
								@endif
							</p>
							
							@endif

						</div>
						<a class="btn-customize glyphicon glyphicon-shopping-cart cart-symbol point-cursor adjust" data-btn="cart-toggle"></a>
					</div>
					<div class="row">
					<nav class="navbar navbar-default" role="navigation">
								<!-- Brand and toggle get grouped for better mobile display -->
								
							
								<!-- Collect the nav links, forms, and other content for toggling -->
								<div class="collapse navbar-collapse menu-drop">
									<ul class="nav navbar-nav">
									@if(\App\Models\Page::all())
										
									
										@foreach(\App\Models\Page::orderby('nav_index','asc')->get()->all() as $nav)
											@if(!array_key_exists("child",$nav))
												<li><a href="{{url('page',$nav->slug)}}">{{$nav->name}}</a></li>
											@else
												<li class="pos-rel dropdown-menu-2"><a href="{{$nav['url']}}">{{$nav["title"]}}</a>
													<ul class="dropdown list-unstyled pos-abs">
														@foreach($art_cat as $child)
														<li><a href="{{ action('ProgramController@show',$child->slug)}}">{{$child->name}}</a></li>
														@endforeach
													</ul>
												</li>
											@endif
										@endforeach
									@endif

									</ul>
									<ul class="nav navbar-nav navbar-right">
											
											
											<li>
												{!! Form::open(["method"=>'GET',"action"=>['SearchController@advancedSearchResult'],'class'=>'navbar-form']) !!}
													<div class="form-group text-center">
														{!! Form::input('text','search',null,['class'=>' form-control','placeholder'=>'Търсене...']) !!}

														<button type='submit' class="btn custom-search form-control">
 															 <span class="glyphicon glyphicon-search"></span>&nbsp;
														</button>
													</div>	

												{!! Form::close() !!}
											</li>
											<li><a href="{!! URL::to('/search/advanced/options') !!}">Разширено търсене</a></li>
									</ul>
								</div><!-- /.navbar-collapse -->
							</nav>

						</div>

					</div>
				
				</div>

</header>
