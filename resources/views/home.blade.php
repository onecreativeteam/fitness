@extends('layouts.app')

@section('content')
  <section class="col-md-8 section"><!--SECTION-->
                       @if(isset($result['heading']))
                        <div class="row">
                            <h1 class="header-border margin-top-res">{{$result['heading']}}</h1>
                        </div>
                        <div class="row push-down">
                            <div class="col-sm-4 text-center"><img src="{{asset($result['picture'])}}" class="img-responsive img-thumbnail  img-max push-down"> </div>
                            <div class="para"> {!! $result['content'] !!} </div>

                        </div>
                        @else
                        	Still in DEVELOPMENT
                        @endif
                    </section><!--/SECTION-->
@endsection
