@extends("layouts.app")

@section("content")

		
	
<section class="col-md-8 section"><!--SECTION-->
	<div class="row">
		<h1 class="header-border margin-top-res">{{$user->name}}</h1>
	</div>
	<div class="row push-down">
		<div class="col-sm-5  profile-picture text-center">
			<img src="{{URL::to($user->picture)}}" alt="" class="img-responsive thumbnail text-center">
		</div>
		<div class="col-sm-7">	
			<ul class="contact-list" data-info="profileOptions">
				<li>
					<span class="">E-mail: <span class="mail">{{$user->email}}</span></span>
				</li>
				<li>
					<span class="">Телефонен номер: <span class="phone">{{$user->phone}}</span></span>
				</li>
				<li>
					<span class="">Адрес: <span class="address">{{$user->address}}</span></span>
				</li>
				<li class="response-li-msg">
				</li>
				<li class="response-li-msg-error">
				</li>

				<li>
					<a href="" id='userAddressChange'>Смяна на адреса</a>
				</li>
				<li>
					<a href="" id='userPhoneChange'>Смяна на телефона</a>
				</li>
				<li>
					<a href="" id='userPictureChange'>Смяна на профилната снимка</a>
				</li>
				<li>
					<a href="" id='userMailChange'>Смяна на майла</a>
				</li>
				<li>
					<a href="" id='userPasswordChange'>Смяна на паролата</a>
				</li>
			</ul>
		</div>
		
	</div>
	<div class="col-md-12" id="responsiveForm" data-state="closed">
		{!! Form::open(['method'=>'PATCH','action'=>['UserController@update',$user->id],'files'=>true]) !!}
				<span class="pull-right glyphicon glyphicon-remove removeBtn"></span>
				<div class="form-inputs form-group"><label for="address">Смяна на </label> <input  class="form-control"></div>
				<div class="text-center">
				{!! Form::submit('Обнови',['class'=>'btn-custom btn-large']) !!}
				</div>			
		{!! Form::close() !!}
		
	</div>
</section>
@stop
@section('side_nav')
 @include('parts.side_menu')
@stop