
				@if(count($errors))
					<ul class="alert">
						@foreach($errors->all() as $err)
							<li>{{$err}}</li>
						@endforeach
					</ul>
				
				@endif