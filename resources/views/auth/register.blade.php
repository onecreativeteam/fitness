@extends('layouts.app')

@section('content')



<section class="col-md-8 section"><!--SECTION-->
                    <div class="row">
                        <h1 class="header-border margin-top-res">Регистрация</h1>
                    </div>
                    <div class="row push-down">
                        
                        {!! Form::open(["url"=>"register","method"=>"POST","class"=>"inner-form"]) !!}
                            
                            <!--username-->
                            <div class="form-row">
                            
                                {!! Form::label("name","Име:")!!}
                                {!! Form::text("name",null,['class'=>'cs-input']) !!}
                            
                            </div><!--username-->
                            <div class="form-row">
                            
                                {!! Form::label("phone","Телефонен номер:")!!}
                                {!! Form::text("phone",null,['class'=>'cs-input']) !!}
                            
                            </div><!--username-->
                            <!--password-->
                            <div class="form-row">
                                {!! Form::label('password',"Парола") !!}
                                {!! Form::password('password',["class"=>'cs-input']) !!}
                            </div><!--password-->
                            <div class="form-row">
                                {!! Form::label('password',"Потвърди Паролата") !!}
                                {!! Form::password('password_confirmation',["class"=>'cs-input']) !!}
                            </div><!--password-->
                            <!--Address-->
                            <div class="form-row">
                                {!!Form::label("address","Адрес")!!}
                                {!!Form::textarea("address",null,['class'=>'cs-input']) !!}
                            </div><!--E-mail-->
                            <div class="form-row">
                                {!!Form::label("email","Е-майл")!!}
                                {!!Form::email("email",null,['class'=>'cs-input']) !!}
                            </div><!--E-mail-->

                         
                            {!! Form::submit("Регистрация" , ["class"=>"btn-custom btn-large"]) !!}
                             
                            @if($errors->all())
                                
                               <p class="alerted text-left">
                                @foreach($errors->all() as $err)
                                    {{$err}}
                                    <br> 
                                @endforeach                                   
                               </p>
                            @endif
                            
                        {!! Form::close() !!}
                    </div>
                </section><!--/SECTION-->





@endsection
