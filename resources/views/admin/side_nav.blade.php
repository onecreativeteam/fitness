<aside class="side-menu back-anim">
		<ul class="unstyled-list unstyled-link">
			
		
			<li><a href="{{ URL::to('/') }}/admin/users">Потребители</a></li>
			<li><a href="{{ URL::to('/') }}/admin/pages">Страници</a></li>
			
			<li class="drop-down"><a href="{{ URL::to('/') }}/admin/products">Продукти</a>
				<ul>
					<li><a href="{{url('admin/products','category')}}">Категории</a></li>
					<li><a href="{{url('admin/products','brand')}}">Марки</a></li>
				</ul>
				</li>
			<li class="drop-down"><a href="{{ URL::to('/') }}/admin/programs">Програми</a>
					<ul>
					<li><a href="{{url('admin/programs','category')}}">Категории</a></li>
					</ul>
			</li>
			<li><a href="{{ URL::to('/') }}/admin/gallery">Галерия</a></li>
			<li><a href="{{ URL::to('/') }}/admin/option">Опции</a></li>
			<li><a href="{{ URL::to('/') }}/admin/mails">Съобщения<span class="pull-right">{{count($new)}}</span></a></li>


			
		</ul>



</aside>