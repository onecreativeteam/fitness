
<h2>Слайдер <!-- <a href="#" data-type="add-slide" class="btn anim">Добави слайд</a> Still in progress -->: </h2>
<div class="row" id="slider_edit">

	@foreach( $page->slides as $slide)
		
		<div class="slide_preview" data-id="{{$slide->id}}">
				<img class="" src="{{ URL::to('/') }}{{$slide->path}}" >
				<div class="params" style='display:none' data-path="{{ URL::to('/') }}{{$slide->path}}" data-id="{{$slide->id}}" data-title="{{$slide->title}}" data-content="{{$slide->content}}" ></div>	
				<a  data-btn="edit-slide"  href="#modal" class="btn anim">Редакция</a>
	
		</div>

	@endforeach
</div>
