@extends('layouts.app_admin')
@section('special')
		 {!! Html::script('js/admin/tinymce/js/tinymce/tinymce.min.js') !!}
		  @include('scriptTyni')
	@stop
@section("content")
		
			{!! Form::model($page,["method"=>"PATCH",'action'=>['Admin\AdminPagesController@update',$page->id]]) !!}

					@include('admin.default._form')
					<div class="form-control">	
						{{ Form::label('slug','Слуг за страницата:')}}
						{{ Form::input('text','slug')}}
					</div>
					@if($page->has_slider)
						@include('admin.default._edit_slider')
					@endif
					
					{!! Form::submit('Update') !!}			
			{!! Form::close() !!}

			<div style='display:none'>
			<div id='modal'  class='custom-modal' style='padding:10px; text-align: center; background:#fff;'>
					{!! Form::open(["method"=>'PATCH','files'=>true,"id"=>'updateForm']) !!}
							
							
							
							<img class="" src="" >
							
							{!! Form::hidden('id') !!}	
							<div class="form-control">	
								{!! Form::label('file',"Снимка за слайда :") !!}	
								{!! Form::file("image",['id'=>'image']) !!}	
							</div>
							<div class="form-control">
							{!! Form::label('title',"Заглавие: ") !!}		
							{!! Form::input('text',"title") !!}	
							</div>
							<div class="form-control">	
							{!! Form::label('content',"Съдържание: ") !!}		
							{!! Form::input('text',"content") !!}	
							</div>
							<button  class="btn" data-btn="ajax">Submit</a>
		
								
					{!! Form::close() !!}
					
				
					
			</div>
			
		</div>
		




@stop