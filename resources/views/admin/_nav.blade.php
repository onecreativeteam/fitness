<nav class="navbar clearfix back-anim">
	<ul class="navbar-left unstyled-list unstyled-link">
		<li><a href="{{action('Admin\AdminController@index')}}">Администрация</a></li>
		<li><a href="{{ URL::to('/') }}">Преглед</a></li>
	</ul>


	<ul class="navbar-right unstyled-list unstyled-link">
		<li><a href="{{action('Admin\AdminController@index')}}">
		@if(count($admin))
		{{$admin->name}}
		@endif
		</a></li>
		<li><a href="{{url('/logout')}}">Изход</a></li>
	</ul>

</nav>