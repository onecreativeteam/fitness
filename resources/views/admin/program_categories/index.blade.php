
@extends('layouts.app_admin')



@section('content')
<h2>Листинг
	
		Категории Програми

	
	 {!! Html::linkAction('Admin\AdminProgramCategoryController@create',"Добави",[],['class'=>'btn anim'])!!} :</h2>	
		
		@if(count($result))

			@foreach($result as $row)
				
				<div class="container">
				
				
						<div class="prod_content">
						<h4 class='name'>Име: {{$row->name}} </h4>
					
						<h4 class='name'>Слуг: {{$row->slug}}</h4>
							<div class="btn_cont">
										
								{!! Html::linkAction('Admin\AdminProgramCategoryController@edit',"Edit", [$row->slug] ,['class'=>'edit anim'])!!}
								{!! Form::open(['method'=>'DELETE','action'=>['Admin\AdminProgramCategoryController@destroy',$row->slug ]]) !!}
								{!! Form::submit("Delete",['class'=>'del anim',"data-btn"=>'destroy']) !!}
								{!! Form::close() !!}
								
							</div>
						</div>
				
				
				
			</div>

		
			@endforeach
			
		@else
			<p class="warning">Няма налична информация</p>
		@endif
	
@stop