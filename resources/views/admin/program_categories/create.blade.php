@extends('layouts.app_admin')


@section('content')


		
			<h1>Добавяне на категория продукти</h1>
				
				@include('admin._err')
				
			{!! Form::open(['action'=>'Admin\AdminProgramCategoryController@store','enctype'=>'multipart/form-data']) !!}
			
				@include('admin.category.form')
				
				{!! Form::submit('Запази',['data-btn'=>"store"]) !!}
		
			{!! Form::close() !!}


			{!! Html::script("js/admin/adm-java.js") !!}


@stop