@extends('layouts.app_admin')
@section('special')
		{!! Html::script('js/admin/tinymce/js/tinymce/tinymce.min.js') !!}
		@include('scriptTyni')
@stop

@section('content')
	<h1>Създай нова страница</h1>
	{!! Form::open(['action'=>['Admin\AdminPagesController@store'],'files'=>true]) !!}
		@include('admin.pages._form')
		<div id="temps"></div>
		{!! Form::submit('Submit') !!}	
	{!! Form::close() !!}
	
	

@stop