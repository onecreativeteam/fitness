@extends('layouts.app_admin')


@section('content')
		<h1>Страници {!! Html::linkAction('Admin\AdminPagesController@create',"add", [] ,['class'=>'btn anim']) !!}</h1>
		@foreach($result as $res)

			<div class="prod_content">
				<div class="description">
						<div class='name'>Име: {{$res->name}}</div>
						<div class='name'>Мета-Заглавие: {{$res->tittle}}</div>
						<div class='name'>Мета-Описание: {{$res->meta_descr}}</div>
						<div class='name'>Шаблон: {{$res->template}}</div>
						
						<div class="btn_cont">
							{!! Html::linkAction("Admin\AdminPagesController@edit", 'Edit',[$res->id],['class'=>'edit anim']) !!}
							{!! Form::open(['method'=>'DELETE','action'=>['Admin\AdminPagesController@destroy',$res->id]]) !!}
								{!! Form::submit('Delete',['class'=>'del anim']) !!}			
							{!! Form::close() !!}
						</div>
					</div>
			</div>

		@endforeach
@stop