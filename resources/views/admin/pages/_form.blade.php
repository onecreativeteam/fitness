<div class="form-control">			
	{!! Form::label('name','Заглавие:') !!}
	{!! Form::input('text','name') !!}
</div>

<div class="form-control">
	{!! Form::label('tittle','Мета-заглавие') !!}
	{!! Form::input('text','tittle') !!}
</div>
<div class="form-control">
	{!! Form::label('nav_index','Индекс на менюто: ') !!}
	{!! Form::input('number','nav_index') !!}
</div>
<div class="form-control">
	{!! Form::label('meta_descr','Мета-описание') !!}
	{!! Form::input('text','meta_descr') !!}
</div>
<div class="form-control">
	{!! Form::label('content','Съдържание') !!}
	{!! Form::textarea('content', null ,['id'=>'textBox']) !!}
</div>
<div class="form-control">
	{!! Form::label('has_slide','Слайдер:') !!}
	{!! Form::checkbox('has_slider' )!!}
</div>
<div class="form-control">
	{!! Form::label('template','Шаблон:') !!}
	{!! Form::select('template',$templates) !!}
</div>
			