<div class="form-control">
				{!! Form::label("name","Име:") !!}
				{!! Form::input("text","name") !!}
				</div>
				<div class="form-control">
				{!! Form::label("slug","Slug референция:") !!}
				{!! Form::input("text",'slug') !!}
				</div>
				<div class="form-control">
				{!! Form::label("description","Описание:") !!}
				{!! Form::textarea('description') !!}
				</div>
				<div class="form-control">
				{!! Form::label("picture","Снимка:") !!}
				{!! Form::file('picture') !!}
				</div>
				<div class="form-control">
				{!! Form::label("price","Цена:") !!}
				{!! Form::input('text','price') !!}
				</div>
				<div class="form-control">
				{!! Form::label("quantity","Налично Количество:") !!}
				{!! Form::input('number','quantity') !!}
				</div>
				<div class="form-control">
				{!! Form::label("brand","Марка") !!}
				{!! Form::select('brand_id',$brands,null,['id'=>'brand']) !!}
				</div>
				<div class="form-control">
				{!! Form::label("category","Категория") !!}
				{!! Form::select('category_id',$category,null,['id'=>'category']) !!}
				</div>
				<div class="">
				{!! Form::label("promotion","Промоция") !!}
				{!! Form::checkbox('promotion') !!}
				</div>
				
				
				{!! Form::submit('Запази',['data-btn'=>'edit']) !!}
				