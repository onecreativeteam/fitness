@extends('layouts.app_admin')


@section('content')
		
			<h1>Редакция на {{$result->name}}</h1>
			<div class="thumbnail">
				<img src="{{asset($result->picture)}}" alt="">
			</div>
				@include('admin._err')
			{!! Form::model($result,['method'=>'PATCH','action'=>['Admin\AdminProductController@update',$result->slug],'enctype'=>'multipart/form-data']) !!}
				@include('admin.product.form')
			{!! Form::close() !!}





@stop