@extends('layouts.app_admin')



@section('content')
		@include('errors._err')
	@if(count($result)>0)
	<h1> Продукти {!! Html::linkAction('Admin\AdminProductController@create',"Добави",[] ,['class'=>'btn anim'])!!}</h1>
		@foreach($result as $line)
				
			<div class="prod_content">
				<img src="{{asset($line->picture)}}" alt="">
				<div class="description">
				<h4 class='name'>Име: {{$line->name}}</h4>
				<h4 class='name'>Слуг: {{$line->slug}}</h4>
				<h4 class='name'>Промоция:
										@if($line->promotion==1)
										   Промоционален
										@else
											Нормален
										@endif	


				</h4>
				<h4 class='name'>Марка: {{$line->brand->name}}</h4>
				<p class='desc'><b>Описание:</b>
				{{$line->description}}
				</p>
				</div>
				<div class='price'> Цена: {{$line->price}} </div>
				<div class="btn_cont">
							
						{!! Html::linkAction('Admin\AdminProductController@edit',"Edit", [ $line->slug] ,['class'=>'edit anim'])!!}
						{!! Form::open(['method'=>'DELETE','action'=>['Admin\AdminProductController@destroy', $line->slug ]]) !!}
						{!! Form::submit("Delete",['class'=>'del anim']) !!}
						{!! Form::close() !!}
				</div>
			</div>
		@endforeach
	@else
		<h1> Все още няма добавени продукти {!! Html::linkAction('Admin\AdminProductController@create',"Добави",[] ,['class'=>'btn anim'])!!}</h1>
	@endif
	
@stop