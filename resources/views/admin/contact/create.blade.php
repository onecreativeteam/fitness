@extends('layouts.app_admin')



@section('content')

	{!! Form::open(['action'=>['HomeController@create',$name]]) !!}
		<div class="form-control">
				{!! Form::label('email',"Фирмен Е-майл: ")!!}
				{!! Form::email("email")!!}
		</div>
		<div class="form-control">
				{!! Form::label("phone","Фирмен Телефон:") !!}			
				{!! Form::input('text','phone',null,["maxlength"=>10]) !!}
		</div>
		<div class="form-control">
				{!! Form::label('address',"Фирмен адрес:") !!}
				{!! Form::input('text',"address")!!}
		</div>
		{!! Form::submit("Запази") !!}
	{!! Form::close() !!}

@stop