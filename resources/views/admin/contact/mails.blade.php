@extends('layouts.app_admin')
	
@section('content')
	<h1>Съобщения към администратора</h1>
	@if(count($mails))
		<table class='table_admin'>	
			<thead>	
				<tr>
					<th>От</th>	
					<th>Тема</th>	
					<th>Съобщение</th>
					<th>Изпратено</th>		
				</tr>
			</thead>
			<tbody>	
		@foreach( $mails as $mail)
				<tr class="readMsg anim
					@if($mail->unread !=0 )
						unread
					@else 
						read
					@endif
					">
					<td>{{$mail->sender}}</td>
					<td>{{$mail->subject}}</td>
					<td>{{substr($mail->text,0,100)}}...</td>
			
					<td>
						{!! Form::open(['method'=>'GET','action'=>['ContactMailController@show',$mail->id]]) !!}	
						
							<p class="small-text">{{$mail->created_at->format("Y-m-d")}}</p>
						
						{!! Form::close() !!}
					</td>	
				</tr>

		@endforeach
			</tbody>
		</table>

	@else
		<div class="error"> 
			<p class="warning">
				Няма изпратени съобщения към администраторите до този момент!!!
			</p>
		</div>
	@endif

@stop