@extends('layouts.app_admin')
@section('special')
		 {!! Html::script('js/admin/tinymce/js/tinymce/tinymce.min.js') !!}
		  @include('scriptTyni')
	@stop
@section("content")

	{!! Form::model($page,['method'=>'PATCH','action'=>['Admin\AdminPagesController@update',$page->id]]) !!}
		@include('admin.default._form')
	
		<div class="form-control">	
				{{ Form::label('slug','Слуг за страницата:')}}
				{{ Form::input('text','slug')}}
		</div>
		<div class="form-control">
				{!! Form::label('mail',"Фирмен Е-майл: ")!!}
				{!! Form::email("mail" ,$result['mail'])!!}
		</div>
		<div class="form-control">
				{!! Form::label("phone","Фирмен Телефон:") !!}			
				{!! Form::input('text','phone',$result['phone']) !!}
		</div>
		<div class="form-control">
				{!! Form::label('address',"Фирмен адрес:") !!}
				{!! Form::input('text',"address",$result['address'])!!}
		</div>
		<div class="form-control">
				{!! Form::label('longitude',"Longitude:") !!}
				{!! Form::input('text',"longitude",$result['longitude'])!!}
		</div>
		<div class="form-control">
				{!! Form::label('latitude',"Latitude:") !!}
				{!! Form::input('text',"latitude",$result['latitude'])!!}
		</div>

		{!! Form::submit("Запази") !!}
	
	
	{!! Form::close() !!}
	
@stop