@extends('layouts.app_admin')
	
@section('content')
	<h1>Съобщения към администратора </h1>
	<h3><a href="{{ URL::previous()}}" class="btn btn-large">Назад</a></h3>
	<div class="row">
		<div class="container">
					<h3>Подател: {{$mail->sender}}</h3>
					<h3>Тема: {{$mail->subject}}</h3>
					<h3>Съобщение:</h3>
					<div class="msgText">
						<p>{{$mail->text}}</p>
					</div>
					
		</div>
	</div>

@stop