@extends('layouts.app_admin')
@section('special')
		 {!! Html::script('js/admin/tinymce/js/tinymce/tinymce.min.js') !!}
		  @include('scriptTyni')
	@stop

@section('content')
		
		{!! Form::model($program,['method'=>'PATCH','action'=>['Admin\AdminProgramController@update',$program->slug],'files'=>true]) !!}
		@include('admin.program._form')
		{!! Form::close() !!}

@stop