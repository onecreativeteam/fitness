@extends('layouts.app_admin')

@section('content')

	<div>

	
		@if(count($programs))
		<h1>Статии {!! Html::linkAction('Admin\AdminProgramController@create',"Добави", [] ,['class'=>'btn anim'])!!}</h1>
				@foreach($programs as $program)

							<div class="prod_content">
						<img src="{{asset($program->image_path)}}" alt="">
						<div class="description">
						<h4 class='name'>Име: {{$program->name}}</h4>
						<h4 class='name'>Слуг: {{$program->slug}}</h4>
				
						<h4 class='name'>Категория: 
							@foreach($program->categories as $category)
								{{$category->name}}
							@endforeach
							

							</h4>
						<p class='desc'><b>Описание:</b>
						{{$program->short_descript}}
						</p>
						</div>
						<div class="btn_cont">
									
								{!! Html::linkAction('Admin\AdminProgramController@edit',"Edit", [ $program->slug] ,['class'=>'edit anim'])!!}
								{!! Form::open(['method'=>'DELETE','action'=>['Admin\AdminProgramController@destroy', $program->slug]]) !!}
								{!! Form::submit("Delete",['class'=>'del anim']) !!}
								{!! Form::close() !!}
						</div>
					</div>
				@endforeach
		@else
			<h1>Няма добавени статии {!! Html::linkAction('Admin\AdminProgramController@create',"Добави", [] ,['class'=>'btn anim'])!!}</h1>
		@endif
	</div>
@stop