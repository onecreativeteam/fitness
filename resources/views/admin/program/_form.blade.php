					<div class="form-control">
						{!! Form::label('name',"Име на статията:") !!}
						{!! Form::input('text',"name") !!}

					</div>
					<div class="form-control">
						{!! Form::label("category","Категория: ") !!}
						{!! Form::select('category_id',$categories,null,['id'=>'category']) !!}
					</div>
					<div class="form-control">
						{!! Form::label('slug',"Slug на статията:") !!}
						{!! Form::input('text',"slug") !!}

					</div>
					<div class="form-control">
						{!! Form::label('short_descript',"Съдържание на статията") !!}
						{!! Form::textarea('short_descript',null,['rows'=>'3']) !!}

					</div>
					<div class="form-control">
						{!! Form::label('picture',"Thumbnail снимка") !!}
						{!! Form::file('picture') !!}

					</div>
					<div class="form-control">
						{!! Form::label('textBox',"Съдържание на статията") !!}
						{!! Form::textarea('text',null,['id'=>'textBox']) !!}

					</div>
					
					{!! Form::submit('Публикувай') !!}