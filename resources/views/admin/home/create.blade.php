@extends('layouts.app_admin')
@section('special')
		 {!! Html::script('js/admin/tinymce/js/tinymce/tinymce.min.js') !!}
		  @include('scriptTyni')
	@stop
@section("content")
	{!! Form::open(['method'=>'POST','action'=>['HomeController@create',$name],'files'=>true]) !!}
				
				@include('admin.home.form')
	
	{!! Form::close() !!}
	
@stop