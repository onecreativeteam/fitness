@extends('layouts.app_admin')


@section('content')
		
			<h1>Редакция на категория</h1>
				
				@include('admin._err')
			{!! Form::model($result,['method'=>'PATCH','action'=>['Admin\AdminProductCategoryController@update', $result->slug],'enctype'=>'multipart/form-data']) !!}
				@include('admin.category.form')
				
				{!! Form::submit('Запази',['data-btn'=>"edit"]) !!}
			
			{!! Form::close() !!}


			{!! Html::script("js/admin/adm-java.js") !!}


@stop