@extends('layouts.app_admin')


@section("content")
	{!! Form::open(array('action'=>'GalleryController@save','method'=>'POST', 'files'=>true)) !!}
		{!! Form::file('images[]', array('multiple'=>true)) !!}
		{!! Form::submit("Запази всички снимки") !!}
	{!! Form::close() !!}

@stop