@extends('layouts.app_admin')

@section('special')
	{!! Html::script("js/admin/dropzone.js") !!}
@stop

@section('content')
<div class="prod_content" style="border:none;">
	<form action="/file-upload" class="dropzone" id="my-awesome-dropzone">
       <input name="file" type="file" multiple />
      </form>
	</div>


	
						@if(count($result)>0)
							@foreach($result as $res)
							<div class="gallery_image">
							<img src="{{ asset($res->img_path)}}" class="img-responsive img-thumbnail  img-max push-down">
							<div class="btn_cont">
								{!! Form::open(['method'=>'DELETE','action'=>['GalleryController@destroy', $res->id]]) !!}
								{!! Form::submit("Delete",['class'=>'del anim']) !!}
								{!! Form::close() !!}
							</div>
							</div>
								
								
			
							@endforeach

						@else
							<p class=" alert">Няма налични снимки323232.</p>
						@endif
					@if($result->render())
						@include('parts.pagination')
					@endif	

				@include('parts.big-pic')
@stop