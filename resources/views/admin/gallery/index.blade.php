@extends('layouts.app_admin')

@section('special')
	{!! Html::style("css/admin/dropzone.css") !!}
	{!! Html::script("js/admin/dropzone.js") !!}
@stop

@section('content')
<div class="prod_content" style="border:none;">
			<form action="{{ url('admin/gallery')}}" class="dropzone" id="my-awesome-dropzone">
				 {{ csrf_field() }}
			</form>

	</div>


	
						@if(count($pictures)>0)
							@foreach($pictures as $res)
							<div class="gallery_image">
								<img src="{{ asset($res->img_path)}}" class="img-responsive img-thumbnail  img-max push-down">
								<div class="btn_cont">
									{!! Form::open(['method'=>'DELETE','action'=>['GalleryController@destroy', $res->id]]) !!}
									{!! Form::submit("Delete",['class'=>'del anim']) !!}
									{!! Form::close() !!}
								</div>
							</div>
								
								
			
							@endforeach

						@else
							<p class=" alert">Няма налични снимки.</p>
						@endif
					@if($pictures->render())
						@include('parts.pagination_gallery')
					@endif	

				@include('parts.big-pic')
@stop