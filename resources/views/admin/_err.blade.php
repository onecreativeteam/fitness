@if(Session::has('added'))
					<h2 class='alert'>{{Session::get('added')}}</h2>
					
				@endif
				@if(count($errors)>0)
					<ul class="alert">
						@foreach($errors->all() as $err)
							<li>{{$err}}</li>
						@endforeach
					</ul>
				
				@endif