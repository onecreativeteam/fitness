@extends("layouts.app_admin")

@section("content")

		<h2>Листинг
	
		 Марки
	
	 {!! Html::linkAction('Admin\AdminProductBrandController@create',"Добави" ,[],['class'=>'btn anim'])!!} :</h2>	
		
		@if(count($result))
<div class="container">
			@foreach($result as $brand)
				
				<div class="row">
						<div class="prod_content">
						<h4 class='name'>Име: {{$brand->name}} </h4>
					
						<h4 class='name'>Слуг: {{$brand->slug}}</h4>
							<div class="btn_cont">
										
								{!! Html::linkAction('Admin\AdminProductBrandController@edit',"Edit", [$brand->slug] ,['class'=>'edit anim'])!!}
								{!! Form::open(['method'=>'DELETE','action'=>['Admin\AdminProductBrandController@destroy',$brand->slug ]]) !!}
								{!! Form::submit("Delete",['class'=>'del anim' , "data-btn"=>'destroy']) !!}
								{!! Form::close() !!}
								
							</div>
						</div>
					</div>
			@endforeach
	</div>		
			
		@else
			<p class="warning">Няма налична информация</p>
		@endif
	
@stop

