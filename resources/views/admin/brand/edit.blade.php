@extends('layouts.app_admin')


@section('content')
		
			<h1>Добавяне на категория</h1>
				
				@include('admin._err')
			{!! Form::model($brand,['method'=>'PATCH','action'=>['Admin\AdminProductBrandController@update',$brand->slug],'enctype'=>'multipart/form-data']) !!}
				@include('admin.category.form')
				
				{!! Form::submit('Запази',['data-btn'=>"edit"]) !!}
				
			{!! Form::close() !!}


			{!! Html::script("js/admin/adm-java.js") !!}


@stop