@extends('layouts.app_admin')


@section('content')


		
			<h1>Добавяне на Марка</h1>
				
				@include('admin._err')
				
			{!! Form::open(['action'=>'Admin\AdminProductBrandController@store']) !!}
				@include('admin.category.form')
				
				{!! Form::submit('Запази' ,['data-btn'=>"store"]) !!}
		
			{!! Form::close() !!}


			{!! Html::script("js/admin/adm-java.js") !!}


@stop