@extends('layouts.app_admin')



@section('content')

	{!! Form::model($result,['method'=>'PATCH',"action"=>['Admin\AdminUsersController@update',$result->id]])!!}
		<div class="form-control">
			{!! Form::label('name','Име:') !!}
			{!! Form::input('text','name') !!}
		</div>
		<div class="form-control">
			{!! Form::label('email','Е-Майл:') !!}
			{!! Form::email('email') !!}
		</div>
		<div class="form-control">
			{!! Form::label('address','Адрес') !!}
			{!! Form::textarea('address')!!}
		</div>
		<div class="form-control">
			{!! Form::label('role',"Роля") !!}
			{!! Form::select('role', ['admin'=>'Администратор',"mod"=>"Модератор",'client'=>'Клиент']) !!}
		</div>
		 {!! Form::submit('Запази') !!}
	{!! Form::close()!!}
@stop