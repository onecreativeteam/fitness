@extends("layouts.app_admin")


@section('content')

		@if(count($result)>0)
				@foreach($result as $line)
				<div class="prod_content">
				
							<span>Име: {{$line->name}}</span>
							<span>Роля: {{$line->role}}</span>
							<span>Е-майл: {{$line->email}}</span>
							<span>Адрес: {{$line->address}}</span>
							
							@can('edit',\Auth::user())
							<div class="btn_cont">
							{!! Html::linkAction('Admin\AdminUsersController@edit',"Edit",[$line->id],['class'=>'edit anim']) !!}
								{!! Form::open(['method'=>'DELETE','action'=>['Admin\AdminUsersController@destroy', $line->id]]) !!}
								{!! Form::submit("Delete",['class'=>'del anim']) !!}
							{!! Form::close() !!}
							</div>
							@endcan
						
			
				</div>


				@endforeach			
		@endif

@stop