
@extends('layouts.app_admin')



@section('content')
<h2>Листинг
	@if($type=="brand")
		 Марки
	@else
		Категории

	@endif
	 {!! Html::linkAction('Admin\AdminCategoryController@create',"Добави", $type ,['class'=>'btn anim'])!!} :</h2>	
		
		@if(count($data))

			@foreach($data as $line)
				
				<div class="container">
				
				
						<div class="prod_content">
						<h4 class='name'>Име: {{$line->name}} </h4>
					
						<h4 class='name'>Слуг: {{$line->slug}}</h4>
							<div class="btn_cont">
										
								{!! Html::linkAction('Admin\AdminCategoryController@edit',"Edit", [$type,$line->slug] ,['class'=>'edit anim'])!!}
								{!! Form::open(['method'=>'DELETE','action'=>['Admin\AdminCategoryController@destroy', $type,$line->slug ]]) !!}
								{!! Form::submit("Delete",['class'=>'del anim']) !!}
								{!! Form::close() !!}
								
							</div>
						</div>
				
				
				
			</div>

		
			@endforeach
			
		@else
			<p class="warning">Няма налична информация</p>
		@endif
	
@stop