@extends('layouts.app_admin')


@section('content')
		
			<h1>Добавяне на категория</h1>
				
				@include('admin._err')
			{!! Form::model($result,['method'=>'PATCH','action'=>['Admin\AdminCategoryController@update',$type,$result->slug],'enctype'=>'multipart/form-data']) !!}
				@include('admin.category.form')
				<div class="">
				{!! Form::submit('Запази') !!}
				</div>
			{!! Form::close() !!}


			{!! Html::script("js/admin/adm-java.js") !!}


@stop