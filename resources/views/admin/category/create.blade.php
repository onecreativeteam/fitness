@extends('layouts.app_admin')


@section('content')


		
			<h1>Добавяне на категория</h1>
				
				@include('admin._err')
				
			{!! Form::open(['action'=>'Admin\AdminCategoryController@store','enctype'=>'multipart/form-data']) !!}
			<div class="form-control">
					{!! Form::label("controller","Добави :") !!}
					
					{!! Form::select('controller',$models,null,['id'=>'controller']) !!}
					</div>
				@include('admin.category.form')
				<div class="">
				{!! Form::submit('Запази') !!}
				</div>
			{!! Form::close() !!}


			{!! Html::script("js/admin/adm-java.js") !!}


@stop