@extends('layouts.app')



	@section("content")
		<section class="col-md-8 section"><!--SECTION-->
            <div class="row">
                <h1 class="header-border margin-top-res"> {{$program->name}}</h1> 
                <div class="col-xs-12 rating-form">
                 {!! Form::open(['action'=>['ProgramRatingController@store']]) !!}
                     {!! Form::input('hidden','program_id',$program->id) !!}
                            <ul class="rating-box">
                                <li><a href="" class="glyphicon glyphicon-star rating" data-rate='1'></a></li>
                                <li><a href="" class="glyphicon glyphicon-star rating" data-rate='2'></a></li>
                                <li><a href="" class="glyphicon glyphicon-star rating" data-rate='3'></a></li>
                                <li><a href="" class="glyphicon glyphicon-star rating" data-rate='4'></a></li>
                                <li><a href="" class="glyphicon glyphicon-star rating" data-rate='5'></a></li>
                                <li> Гласувай с <span class='will-rate'>0</span> звезди</li>
                                <br>
                                <li> Райтинг:&nbsp;<span class='current-rate'>
                                @if(count($program->rating))
                                {{$program->rating->avg('rate')}}
                                @else
                                0
                                @endif
                                </span></li>
                            </ul>
                    {!! Form::close() !!}
                    <ul><li class='response-li-msg'></li></ul>
                    </div>
                <div class="pull-right">
                    <span>Категория: </span>
                    <a class='pull-right' href="{{action( 'ProgramController@index',[$category->slug])}}"> {{$category->name}}</a> 
                </div>
            </div>
            <div class="row push-down">
               {!! $program->text !!} 
            </div>
        </section><!--/SECTION-->
	@stop
    @section('side_nav')
        @include('parts.side_menu_programs')
    @stop