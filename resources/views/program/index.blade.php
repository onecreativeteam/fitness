@extends('layouts.app')

@section('content')

	<section class="col-sm-12 col-md-8 section"><!--SECTION-->
				
					<div class="row">
						<h2 class="header-border margin-top-res">{{$category->name}}</h2>
					</div>
					<div class="row">
						@if(count($result)>0)
									

							@foreach($result as $res)
									<div class="col-xs-12">
																	
										 <a href="{{action('ProgramController@show',[$category->slug,$res->slug])}}"><!--First News -->
											<div class="thumbnail row">
									{{ Html::image($res->image_path, mb_strtolower($res->name) ,array('class' => 'thumbnail img-responive art-thumb col-md-4')) }}

												
												<h4 class="col-md-8">{{$res->name}}</h4>
											
												<p class="col-md-8 info">{{$res->short_descript}}...</p>
											</div>
										</a>
									</div>
							@endforeach

						@else
							<div class="alert">
								Все ощя няма данни за тази страница!
							</div>
						@endif
					
					</div>
						@if($result->render())
							<div class="row text-center">
								{{$result->render()}}
							</div>
						@endif
				
				</section>

@stop
@section('side_nav')
		@include('parts.side_menu_programs')
@stop