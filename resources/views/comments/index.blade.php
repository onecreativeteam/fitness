<div class="row push-down" id="commentBox">
						<h2 class="header-border  margin-top-res">Коментари <span id="comment-box" class="glyphicon glyphicon-chevron-down pull-right point-cursor"></span></h2>
						<div id="comment-box-toggled" class="para push-down" data-tg="closed" style="display:none">
							@include('comments.form')
						</div>
						<div>
							@if(count($product->direct_comments))
								<ul class=" para" >
									@foreach($product->direct_comments as $comment)
										 <?php $offset = 0; $set=12;  ?>
										 @include('comments.comment_in', ['comment' => $comment])
										
									@endforeach
								</ul>
							@else
								<p class="push-down col-xs-12 para">
									
									 Бъди първият коментирал, натисни върху стрелката в дясно !!!!
									
								</p>
							@endif
						</div>
</div>