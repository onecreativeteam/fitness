@extends("layouts.app")

@section("content")
	<section class="col-sm-12 col-md-8 section"><!--SECTION-->
			<div class="row header-border push-down">
				
					<h1 class=" margin-top-res">Разширено Търсене:</h1>
			
			</div>
			<div class="p-m-reset push-down custom-width">
					@if(isset($options))
						{!! Form::model($options,['method'=>'GET','action'=>'SearchController@advancedSearchResult','id'=>'_advancedSearch','class'=>'push-down']) !!}
					@endif
						{!! Form::open(['method'=>'GET','action'=>'SearchController@advancedSearchResult','id'=>'_advancedSearch']) !!}
						
						{!! Form::input('hidden','sort',0) !!}
						{!! Form::input('text','search',null,['class'=>'form-control']) !!}
						<h3>Търсене в:</h3>
						<div class="row">
								<div class="col-xs-12">	
										<div class="checkbox">
												 <label>{!! Form::checkbox("page") !!} Страници</label>
										</div>
								</div>
								<div class="col col-md-3">
								<h4>Products Categories</h4>
									<?php 	 $count=0;?>
								@foreach($result["products"] as $key => $name)
									<?php 	 $count++;?>
									<div class="checkbox">
									 		<label>{!! Form::checkbox("products[]",$key) !!} {{$name}}</label>
									</div>
									@if($count===10)
										</div>
										<div class="col col-md-3">
									@endif
								@endforeach
								</div>
								<div class="col col-md-3">
									<h4>Programs Categories</h4>
									<?php 	 $count=0;?>
								@foreach($result["programs"] as $key => $name)
										<?php 	 $count++;?>
									<div class="checkbox">
									 		<label>{!! Form::checkbox('programs[]',$key) !!} {{$name}}</label>
									</div>
									@if($count===10)
										</div>
										<div class="col col-md-3">
									@endif
								@endforeach
								</div>
						
							<div class="col col-md-3">
									<h4>Brand</h4>
									<?php 	 $count=0;?>
								@foreach($result["brand_id"] as $key => $name)
										<?php 	 $count++;?>
									<div class="checkbox">
									 		<label>{!! Form::checkbox('brand_id[]',$key) !!} {{$name}}</label>
									</div>
									@if($count===10)
										</div>
										<div class="col col-md-3">
									@endif
								@endforeach
								</div>
							</div>
							<div class="row">	
								<div class="col-xs-12">	
								{!! Form::submit('Търси...',['class'=>'btn-custom btn-large']) !!}			
								</div>
							</div>
					{!! Form::close() !!}
					
			</div>
			<div class="row p-m-reset push-down custom-width">
			@if(isset($results) && count($results))
				<div class="row header-border push-down">
					<div class="col-xs-8">
						<h1 class=" margin-top-res">Резултати:</h1>
					</div>
					
					 <div class="col-xs-4">
					 	<a data-btn='submit' data-target="#_advancedSearch" data-sort='{{$options["sort"]}}'  class="text-right sorting">{{$options['sort_type']}}
						<span class="glyphicon glyphicon-sort pull-right"></span>
					 	</a>
					 </div>			 
			
				</div>
			
					
						
						@foreach($results as $row)

							@if(isset($row["image_path"]))
							
									<div class="col-xs-12">
										
												<a href="{{action('ProgramController@show',[$row['categories'][0]['slug'],$row['slug']])}}"><!--First News-->
													<div class="thumbnail row">
													{{ Html::image($row['image_path'], mb_strtolower($row['name']) ,array('class' => 'thumbnail img-responive art-thumb col-md-4')) }}
														
														<h4 class="col-md-8">Програмата: {{$row['name']}}</h4>
														<p class="col-md-8 info">{!! $row['short_descript'] !!}...</p>
														<p class="col-md-8 info">Created at: <b>{{$row['created_at']}}</b></p>
													</div>
												</a>
											</div>
							@elseif(isset($row["picture"]))
									<div class="col-xs-12">	
																
											<a href="{{action('ProductController@show',$row['slug'])}}"><!--First News-->
													<div class="thumbnail row">
														{{ Html::image($row['picture'], mb_strtolower($row['name']) ,array('class' => 'thumbnail img-responive art-thumb col-md-4')) }}
														<h4 class="col-md-8">Продукта: {{$row['name']}}</h4>
														<p class="col-md-8 info">{{mb_substr($row['description'], 0, 200)}}...</p>
														<p class="col-md-8 info">Created at: <b>{{$row['created_at']}}</b></p>
													</div>
											</a>
									</div>
							@else
										<div class="col-xs-12">								
											<a href="{{action('PagesController@show',[$row['slug']])}}"><!--First News-->
													<div class="thumbnail row">
														
														<h4 class="col-md-8">Страницата: {{$row['name']}}</h4>
														<p class="col-md-8 info">{!! mb_substr($row['content'], 150, 350) !!}...</p>
													</div>
											</a>
									</div>
							@endif
						@endforeach
					@else
					<div class="row ">	
							<p class="alert"> Няма намерени резултати за <b style="font-size:1.3em;">{{$options['search']}}</b></p>
					</div>
					@endif

			</div>
		</section>	

@stop