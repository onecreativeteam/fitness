@extends("layouts.app")

@section("content")

		<section class="col-sm-12 col-md-8 section"><!--SECTION-->
			<div class="row">
				<h1 class="header-border margin-top-res">Търсене:		
						{!! Form::open(['method'=>'GET','action'=>['SearchController@simpleSearch'],'class'=>'form-right']) !!}
							
								{!! Form::hidden("search",$search) !!}
								{!! Form::hidden("sort",$sort)!!}
								{!! Form::submit('Дата') !!}			
						{!! Form::close() !!}
				</h1>

			</div>
			<div class="row p-m-reset push-down custom-width">
					@if(count($result))
						
						@foreach($result as $row)

							@if(isset($row["image_path"]))
									<div class="col-xs-12">
										
												<a href="{{action('ProgramController@showPost',[$row['slug']])}}"><!--First News-->
													<div class="thumbnail row">
														<img class="thumbnail img-responive art-thumb col-md-4"  src="{{URL::to($row['image_path'])}}" alt="{!! mb_strtolower($row['name']) !!}">
														<h4 class="col-md-8">Програмата: {{$row['name']}}</h4>
														<p class="col-md-8 info">{!! $row['short_descript'] !!}...</p>
														<p class="col-md-8 info">Created at: <b>{{$row['created_at']}}</b></p>
													</div>
												</a>
											</div>
							@elseif(isset($row["picture"]))
									<div class="col-xs-12">	
																
											<a href="{{action('ProductController@show',[$row['slug']])}}"><!--First News-->
													<div class="thumbnail row">
														<img class="thumbnail img-responive art-thumb col-md-4"  src="{{URL::to($row['picture'])}}" alt="{!! mb_strtolower($row['name']) !!}">
														<h4 class="col-md-8">Продукта: {{$row['name']}}</h4>
														<p class="col-md-8 info">{{mb_substr($row['description'], 0, 200)}}...</p>
														<p class="col-md-8 info">Created at: <b>{{$row['created_at']}}</b></p>
													</div>
											</a>
									</div>
							@else
										<div class="col-xs-12">								
											<a href="{{action('PagesController@show',[$row['slug']])}}"><!--First News-->
													<div class="thumbnail row">
														
														<h4 class="col-md-8">Страницата: {{$row['name']}}</h4>
														<p class="col-md-8 info">{!! mb_substr($row['content'], 150, 350) !!}...</p>
													</div>
											</a>
									</div>
							@endif
						@endforeach
					@else
						<p class="alert"> Няма намерени резултати за <b style="font-size:1.3em;">{{$search}}</b></p>
					@endif

			</div>
		</section>	

@stop