@extends('layouts.app')

@section('content')


<?php 	$pictures = \App\Models\Gallery::paginate(9); $row=0;?>
			<section class="col-sm-12 col-md-8 section "><!--SECTION-->
					<div class="row">
						<h1 class="header-border margin-top-res">Добре дошли в FitnesX</h1>
					</div>
					<div class="row push-down">
				
					
						@if(count($pictures)>0)
							@foreach($pictures as $picture)
							<?php $row++;?>
						@if($row % 3 === 0)
							<div class="row p-m-reset push-down custom-width"><!--/FIRST PROD-->		
						@endif	
								<div class="col-xs-4 text-center">
									<a class="point-cursor gallery_pictures"  href="{{asset($picture->img_path)}}">
									{{ Html::image($picture->img_path, "" ,array('class' => 'img-responsive img-thumbnail  img-max push-down')) }}
									</a> 
								</div> 
						@if($row % 3 === 0)
							</div>
						@endif
							
							
							@endforeach
						@else
							<p class=" alert">Няма налични снимки.</p>
						@endif
						</div>
					</div>
					@if($pictures->render())
						@include('parts.pagination_gallery')
					@endif	
				</section>

			

@stop
@section('side_nav')
		@include('parts.side_menu')
@stop