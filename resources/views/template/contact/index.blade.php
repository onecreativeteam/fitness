@extends('layouts.app')
	
	@section('special')
	<script src="https://maps.googleapis.com/maps/api/js"></script>
	<script>
      function initialize() {
        var mapCanvas = document.getElementById('map');
        var mapOptions = {
	      center: new google.maps.LatLng({{$result->latitude}},{{$result->longitude}}),
          zoom: 18,
          mapTypeId: google.maps.MapTypeId.SATELLITE
        }
        var map = new google.maps.Map(mapCanvas, mapOptions)
      }
      google.maps.event.addDomListener(window, 'load', initialize);
    </script>
	@stop

@section('content')

	
    <section class="col-md-8 section"><!--SECTION-->
					
					@if(isset($result))
					<div class="row">
						<h1 class="header-border margin-top-res">{{$result->name}}</h1>
					</div>
					<div class="row push-down">
						<div class="col-xs-12">
							<ul class="contact-list">
								<li>
									Е-майл:	{{$result->mail}}
								</li>
								<li>
									Телефон:	(+359){{$result->phone}}
								</li>
								<li>
									Адрес: {{$result->address}}
								</li>

							</ul>
						</div>
					</div>
					{!! Form::open(['action'=>'ContactController@contact']) !!}
							@if(null!=\Session::get('msg'))
								<div class='success'>{{\Session::get('msg')}}</div>
							@endif
							<div class="form-group">
								{!! Form::label('mail','Е-майл: ') !!}
								{!! Form::email('mail',null,['class'=>'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('theme','Тема: ') !!}
								{!! Form::input('text','theme',null,['class'=>'form-control']) !!}
							</div>
							<div class="form-group">
								{!! Form::label('mail_cont','Съдържание: ') !!}
								{!! Form::textarea('mail_cont',null,['class'=>'form-control'])!!}
							</div>
							{!! Form::submit('Изпрати',['class'=>'btn-custom btn-large']) !!}			
					{!! Form::close() !!}
					
					
						<div class="row text-center pull-down push-down success">
						<div id="map" class=""></div>
						</div>
					</div>
					
					@else
						STILL IN DEV
					@endif
				</section><!--/SECTION-->

@stop
@section('side_nav')
		@include('parts.side_menu')
@stop