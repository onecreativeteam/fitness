@extends("layouts.app")

@section('content')
<section class="col-sm-12 col-md-8 section"><!--SECTION-->	
			<div class="row">
				<h2 class="header-border margin-top-res">Цел на тренировката</h2>
			</div>
			<div class="row">
			<?php 	$categories = \App\Models\Program_Category::all(); ?>
					@foreach($categories as $cat)
						<div class="col-xs-12"><a href="{{action('ProgramController@index',$cat->slug)}}" class="btn-custom btn-large text-center">{{$cat->name}}</a></div>		
					@endforeach
			</div>			
</section><!--Sections-->
@stop
@section('side_nav')
		@include('parts.side_menu_programs')
@stop