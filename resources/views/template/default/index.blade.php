@extends('layouts.app')

@section('content')
  <section class="col-md-8 section"><!--SECTION-->
                       @if(isset($result))
                        <div class="row">
                            <h1 class="header-border margin-top-res">{{$result->name}}</h1>
                        </div>
                        <div class="row push-down">
                           
                            <div class="para"> {!! $result->content !!} </div>

                        </div>
                        @else
                        	Still in DEVELOPMENT
                        @endif
                    </section><!--/SECTION-->
@endsection

@section('side_nav')
        @include('parts.side_menu')
@stop
